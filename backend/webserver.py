import tornado.httpserver
import tornado.websocket
import tornado.ioloop
import tornado.web

import socket
import json

"""
"""
class DeepVisWebHandler(tornado.websocket.WebSocketHandler):
	def initialize(self, vis_handler):
		self.__vis_handler = vis_handler

	def open(self):
		print('New connection opened')

		# Get the info about the layers in the model and send it to the front-end.
		layers_info = self.__vis_handler.model_info()
		self.write_message(layers_info)

	def on_message(self, message):
		print('message received: %s' % message)
		json_msg = json.loads(message)

		if json_msg['id'] == 'intra_layer_projection':
			answer = self.__vis_handler.intra_layer_projection(json_msg['layer'], json_msg['sorting'], json_msg['projection'])
		elif json_msg['id'] == 'inter_layer_projection':
			answer = self.__vis_handler.inter_layer_projection(json_msg['projection'])
		elif json_msg['id'] == 'hidden_state_projection':
			answer = self.__vis_handler.hidden_state_projection(json_msg['layer'], json_msg['projection'])
		elif json_msg['id'] == 'activation_dispersion':
			answer = self.__vis_handler.activation_dispersion(json_msg['layer'], json_msg['unit'])
		elif json_msg['id'] == 'activation_scatterplot':
			answer = self.__vis_handler.activation_scatterplot(json_msg['layer1'], json_msg['unit1'], json_msg['layer2'], json_msg['unit2'])

		self.write_message(answer)

	def on_close(self):
		print('connection closed')

	def check_origin(self, origin):
		return True

# Create a connection with front-end server
def create_connection(vis_handler):
	application = tornado.web.Application([(r'/ws', DeepVisWebHandler, {'vis_handler': vis_handler}),])
	http_server = tornado.httpserver.HTTPServer(application)
	http_server.listen(8080)
	myIP = socket.gethostbyname(socket.gethostname())
	print ('*** Websocket Server Started at %s***' % myIP)
	tornado.ioloop.IOLoop.instance().start()