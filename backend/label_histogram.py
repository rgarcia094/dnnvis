import numpy as np

from sklearn.manifold import TSNE, Isomap, LocallyLinearEmbedding
from sklearn.cluster import AgglomerativeClustering

from model_interface import ModelInterface
from dataset import Dataset

"""
	Create histograms measuring the accumulate activation produced by each unit in the model for inputs of every label in the dataset.
"""
class LabelHistogram:
	def __init__(self, model, dataset, epoch=0):
		self.__model = model
		self.__dataset = dataset
		self.__epoch = epoch

		# Number of labels in the dataset.
		self.__n_labels = dataset.total_labels()

		# Append to layer index the start index of each layer in the histogram matrix.
		self.__layer_index = []
		t_units = 0 
		for l in range(self.__model.number_of_layers()):
			self.__layer_index.append(t_units)
			t_units += self.__model.layer_size(l)

		# Matrix containing histograms of all units in the model.
		self.__histograms = np.zeros((self.__model.model_units(), self.__n_labels))

		# List with the order of each unit in their layers. Important when sorting matrix row's
		self.__unit_order = np.zeros((self.__model.model_units(),))

		self.__create_label_histograms()

	# Generate the label histogram for every unit in the model.
	def __create_label_histograms(self):
		# Generate histograms using a balanced dataset with the same number of elements for each label.
		data_x, data_y = self.__dataset.balanced_dataset(elem_per_label=100, test_set=True)

		# Input data has to be updated on each layer, as it's the output of the previous one.
		input_data = data_x

		# Build histograms for all layers
		i = 0
		for l in range(self.__model.number_of_layers()):
			# Calculate activations for the current layer. Input layers return no activations.
			if self.__model.layer_type(l) != 'InputLayer':
				activations = self.__model.layer_activations(l, input_data)
			else:
				activations = input_data

			# Fill the histograms of the current layer.
			for u in range(self.__model.layer_size(l)):
				for c in range(self.__n_labels):
					# If the layer produces high-dimensional activations in each unit, get the mean activation.
					if self.__model.layer_type(l) == 'Conv2D':
						a = activations[(data_y == c)[:, 0], :, :, u]
						a = np.sum(a, axis=(1, 2))
					elif self.__model.layer_type(l) == 'Conv1D':
						a = activations[(data_y == c)[:, 0], :, u]
						a = np.sum(a, axis=1)
					elif self.__model.layer_type(l) == 'LSTM' and self.__model.return_sequences(l):
						a = activations[(data_y == c)[:, 0], :, u]
						a = np.sum(a, axis=1)
					elif self.__model.layer_type(l) == 'Embedding':
						a = activations[(data_y == c)[:, 0], :, u]
						a = np.sum(a, axis=1)
					else:
						a = activations[(data_y == c)[:, 0], u]

					# Stack activations of every element with such label in a bin of the histogram.
					self.__histograms[i+u, c] = float(np.sum(a))

				self.__unit_order[i+u] = u

			# Normalize histograms of the current layer.
			if self.__model.layer_size(l) > 0 and np.max(self.__histograms[i:i+self.__model.layer_size(l), :]) != 0:
				# print('Layer:', l, 'Max:', np.max(self.__histograms[i:i+self.__model.layer_size(l), :]))

				self.__histograms[i:i+self.__model.layer_size(l), :] /= np.max(self.__histograms[i:i+self.__model.layer_size(l), :])
				# mean = np.mean(self.__histograms[i:i+self.__model.layer_size(l), :])
				# stdv = np.std(self.__histograms[i:i+self.__model.layer_size(l), :])
				# self.__histograms[i:i+self.__model.layer_size(l), :] = (self.__histograms[i:i+self.__model.layer_size(l), :] - mean)/stdv

			elif self.__model.layer_size(l) > 0 and np.max(self.__histograms[i:i+self.__model.layer_size(l), :]) == 0:
				print('Layer Histogram max is zero...')

			# Update initial index of next layer.
			i += self.__model.layer_size(l)

			# Update input data for the next layer.
			input_data = activations

		# Reset model, otherwise it may affect next operations.
		self.__model.reset_model_architecture()
		self.__model.load_epoch_weights(self.__epoch)

	# Return the starting and ending indexes of the interval corresponding to the chosen layer's histogram.
	def __layer_interval(self, layer=None):
		if layer == None:
			s = 0
			e = self.__histograms.shape[0]
		elif layer == self.__model.number_of_layers()-1:
			s = self.__layer_index[layer]
			e = self.__model.model_units()
		else:
			s = self.__layer_index[layer]
			e = self.__layer_index[layer+1]

		return s, e

	# Sort the histograms of a chosen layer according to a specified method.
	def sort_histograms(self, layer, method='unit', label=0):
		# Row containing the first histogram of layer.
		s, e = self.__layer_interval(layer=layer)

		# Don't do anything if the layer has no units
		n_units = e - s
		if n_units == 0:
			return

		# Slice the part of the matrix containing histograms of the selected layer.
		h = self.__histograms
		o = self.__unit_order
		m = h[s:e, :]
		l = o[s:e]

		# Decreasingly sort the elements in m according to the chosen label column.
		if method == 'label':
			order = np.argsort(-m[:, label])

		# Cluster the elements using an hierarchical techniques and sort the matrix rows based on the retrieved hierarchy.
		elif method == 'agglomerative':
			c = AgglomerativeClustering(n_clusters=m.shape[0]).fit(m)
			
			merge_order = c.children_
			index_list = [[i] for i in range(m.shape[0])]

			for i, j in merge_order:
				index_list.append(index_list[i] + index_list[j])

			order = index_list[-1]
		
		# Project data to 1D using t-SNE and order elements according to the projection order.
		elif method == 't-sne':
			p = TSNE(n_components=1, perplexity=5, early_exaggeration=50.0, learning_rate=300.0).fit_transform(m)
			order = np.argsort(p.reshape((p.shape[0],)))

		# Project data to 1D using ISOMAP and order elements according to the projection order.
		elif method == 'isomap':
			p = Isomap(n_components=1, n_neighbors=int(n_units*0.2)).fit_transform(m)
			order = np.argsort(p.reshape((p.shape[0],)))

		# Project data to 1D using LLE and order elements according to the projection order.
		elif method == 'lle':
			p = LocallyLinearEmbedding(n_components=1, n_neighbors=int(n_units*0.4), method='ltsa').fit_transform(m)
			order = np.argsort(p.reshape((p.shape[0],)))

		# Sort by the unit number.
		elif method == 'unit':
			order = np.argsort(l)

		# Don't sort the matrix.
		else:
			order = np.array([i for i in range(n_units)])

		# Update matrix and ordering list.
		h[s:e, :] = m[order, :]
		o[s:e] = l[order]

		# Transform back to python list.
		self.__histograms = h
		self.__unit_order = o

	# Project histograms of a layer to 2D, if no layer is specified, project all layers.
	def project_histograms(self, layer=None, method='t-sne'):
		# Get the start and end indexes of the row interval the projection is applied.
		s, e = self.__layer_interval(layer=layer)

		# Don't do anything if the layer has no units
		n_units = e - s
		if n_units == 0:
			return np.array([])

		# If there is only two labels in the dataset, don't project the data.
		if self.__n_labels == 2:
			return self.__histograms[s:e, :]

		# Slice the matrix interval containing the histograms we want to project.
		h = self.__histograms
		m = h[s:e, :]

		# Project data to 2D using the chosen method.
		if method == 't-sne':
			p = TSNE(n_components=2, perplexity=20, early_exaggeration=50.0, learning_rate=300.0).fit_transform(m)
		elif method == 'isomap':
			p = Isomap(n_components=2, n_neighbors=int(n_units*0.2)).fit_transform(m)
		elif method == 'lle':
			p = LocallyLinearEmbedding(n_components=2, n_neighbors=int(n_units*0.4), method='ltsa').fit_transform(m)
		else:
			print('Wrong method name...')

		return p

	# Create dictionary with data;
	def get_data(self, layer=None, sorting='t-sne', projection='t-sne'):
		if layer != None and sorting[:5] == 'label':
			self.sort_histograms(layer, method='label', label=int(sorting[5:]))
		elif layer != None:
			self.sort_histograms(layer, method=sorting)

		data = {}

		if layer == None:
			data['id'] = 'InterLayerProjection'
			data['layer'] = -1
		else:
			data['id'] = 'IntraLayerProjection'
			data['layer'] = layer

		data['epoch'] = self.__epoch
		data['labels'] = self.__n_labels

		# Get the start and end indexes of the row interval the projection is applied.
		s, e = self.__layer_interval(layer=layer)

		data['histograms'] = self.__histograms[s:e].tolist()
		data['unit_order'] = self.__unit_order[s:e].tolist()
		data['layer_start'] = self.__layer_index
		data['projection'] = self.project_histograms(layer=layer, method=projection).tolist()

		return data

	def histograms(self):
		return self.__histograms

	def unit_order(self):
		return self.__unit_order