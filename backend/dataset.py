import numpy as np

from keras.datasets import cifar10, imdb, mnist, reuters
from keras.preprocessing import sequence

"""
	This class stores information regarding the training set which the model was trained.
"""
class Dataset:
	def __init__(self, model_folder):
		self.__folder = model_folder

	# Return the total number of labels in the dataset.
	def total_labels(self):
		if 'cifar10' in self.__folder:
			return 10
		elif 'imdb' in self.__folder:
			return 2
		elif 'mnist' in self.__folder:
			return 10
		elif 'reuters' in self.__folder:
			return 46

	# Number of elements in each input. Use only for RNNs.
	def input_elements(self):
		if 'imdb' in self.__folder:
			return 500
		elif 'reuters' in self.__folder:
			return 500
		else:
			return 0

	# Return a sub-set from either training or test set containing the same amount of elements for all labels.
	def balanced_dataset(self, elem_per_label=20, test_set=True):
		# Get elements from label 0.
		data_x, data_y = self.get_elements_from_label(label=0, n=elem_per_label, test_set=test_set)

		# Append elements from the other labels.
		for l in range(1, self.total_labels()):
			features, outputs = self.get_elements_from_label(label=l, n=elem_per_label, test_set=test_set)

			data_x = np.append(data_x, features, axis=0)
			data_y = np.append(data_y, outputs, axis=0)

		return data_x, data_y

	# Return the first n elements in the test/training set with the label given as parameter.
	def get_elements_from_label(self, label, n=20, test_set=True):
		if 'cifar10' in self.__folder:
			(x_train, y_train), (x_test, y_test) = cifar10.load_data()
			x_train = x_train.astype('float32') 
			x_test = x_test.astype('float32')
			x_train /= np.max(x_train)
			x_test /= np.max(x_test)

		elif 'mnist' in self.__folder:
			(x_train, y_train), (x_test, y_test) = mnist.load_data()

			# MNIST needs an additional dimension.
			x_train = x_train.reshape((x_train.shape[0], x_train.shape[1], x_train.shape[2], 1))
			x_test = x_test.reshape((x_test.shape[0], x_test.shape[1], x_test.shape[2], 1))
			y_train = y_train.reshape((y_train.shape[0], 1))
			y_test = y_test.reshape((y_test.shape[0], 1))
		
		elif 'imdb' in self.__folder:
			(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=5000)
			x_train = sequence.pad_sequences(x_train, maxlen=500)
			x_test = sequence.pad_sequences(x_test, maxlen=500)
			y_train = y_train.reshape((y_train.shape[0], 1))
			y_test = y_test.reshape((y_test.shape[0], 1))

		elif 'reuters' in self.__folder:
			(x_train, y_train), (x_test, y_test) = reuters.load_data(num_words=5000, index_from=0)
			x_train = sequence.pad_sequences(x_train, maxlen=500)
			x_test = sequence.pad_sequences(x_test, maxlen=500)
			y_train = y_train.reshape((y_train.shape[0], 1))
			y_test = y_test.reshape((y_test.shape[0], 1))

		# Return either test or training set.
		if test_set:
			data_x, data_y = x_test, y_test
		else:
			data_x, data_y = x_train, y_train

		# Keep only the first num_elem elements with the desired label
		data_x = data_x[data_y[:, 0] == label][:n]
		data_y = data_y[data_y[:, 0] == label][:n]

		return data_x, data_y

	def get_elements(self, n=200):
		if 'cifar10' in self.__folder:
			(x_train, y_train), (x_test, y_test) = cifar10.load_data()
			x_train = x_train.astype('float32') 
			x_test = x_test.astype('float32')
			x_train /= np.max(x_train)
			x_test /= np.max(x_test)

		elif 'mnist' in self.__folder:
			(x_train, y_train), (x_test, y_test) = mnist.load_data()
			x_train = x_train.reshape((x_train.shape[0], x_train.shape[1], x_train.shape[2], 1))
			x_test = x_test.reshape((x_test.shape[0], x_test.shape[1], x_test.shape[2], 1))
			y_train = y_train.reshape((y_train.shape[0], 1))
			y_test = y_test.reshape((y_test.shape[0], 1))

		elif 'imdb' in self.__folder:
			(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=5000)
			x_train = sequence.pad_sequences(x_train, maxlen=500)
			x_test = sequence.pad_sequences(x_test, maxlen=500)
			y_train = y_train.reshape((y_train.shape[0], 1))
			y_test = y_test.reshape((y_test.shape[0], 1))

		elif 'reuters' in self.__folder:
			(x_train, y_train), (x_test, y_test) = reuters.load_data(num_words=5000, index_from=0)
			x_train = sequence.pad_sequences(x_train, maxlen=500)
			x_test = sequence.pad_sequences(x_test, maxlen=500)
			y_train = y_train.reshape((y_train.shape[0], 1))
			y_test = y_test.reshape((y_test.shape[0], 1))

		data_x = x_test[:n]
		data_y = y_test[:n]

		return data_x, data_y

	# Return a dictionary translating ids to words for a text dataset.
	def id_index(self):
		if 'imdb' in self.__folder:
			# Map word to id.
			word_index = imdb.get_word_index()
			word_index = {k: (v+3) for k, v in word_index.items()}
			word_index["<PAD>"] = 0
			word_index["<START>"] = 1
			word_index["<UNK>"] = 2
			
			# Revert it to map ids to words.
			id_index = {v: k for k, v in word_index.items()}
			return id_index
		else:
			return None

	# # Get n elements from either the test or training set.
	# def get_elements(self, num_elem=100, test_set=True):
	# 	if 'cifar10' in self.__folder:
	# 		(x_train, y_train), (x_test, y_test) = cifar10.load_data()

	# 		# Return either test or training set.
	# 		if test_set:
	# 			data_x, data_y = x_test[:num_elem], y_test[:num_elem]
	# 		else:
	# 			data_x, data_y = x_train[:num_elem], y_train[:num_elem]

	# 		# Image datasets have no word inputs.
	# 		word_to_id, id_to_word = None, None

	# 		# Name of each label in the dataset ordered by label id.
	# 		labels = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']

	# 	elif 'imdb' in self.__folder:
	# 		(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=5000)

	# 		# Every input (review) must have 500 elements (words). Fill empty words with zeroes.
	# 		x_train = sequence.pad_sequences(x_train, maxlen=500)
	# 		x_test = sequence.pad_sequences(x_test, maxlen=500)

	# 		# Return either test or training set.
	# 		if test_set:
	# 			data_x, data_y = x_test[:num_elem], y_test[:num_elem]
	# 		else:
	# 			data_x, data_y = x_train[:num_elem], y_train[:num_elem]

	# 		# Look-up table translating words to integer id's.
	# 		word_to_id = imdb.get_word_index()
	# 		word_to_id["<PAD>"] = 0
	# 		word_to_id["<START>"] = 1
	# 		word_to_id["<UNK>"] = 2

	# 		# Inverse look-up table translating id's to words.
	# 		id_to_word = {v: k for k, v in word_to_id.items()}

	# 		# Name of each label in the dataset ordered by label id.
	# 		labels = ['negative', 'positive']
		
	# 	elif 'mnist' in self.__folder:
	# 		(x_train, y_train), (x_test, y_test) = mnist.load_data()

	# 		# Return either test or training set.
	# 		if test_set:
	# 			data_x, data_y = x_test[:num_elem], y_test[:num_elem]
	# 		else:
	# 			data_x, data_y = x_train[:num_elem], y_train[:num_elem]

	# 		# Image datasets have no word inputs.
	# 		word_to_id, id_to_word = None, None

	# 		# Name of each label in the dataset ordered by label id.
	# 		labels = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
		
	# 	elif 'reuters' in self.__folder:
	# 		(x_train, y_train), (x_test, y_test) = reuters.load_data(num_words=10000, index_from=0)

	# 		# Every input (review) must have 512 elements (words). Fill empty words with zeroes.
	# 		x_train = sequence.pad_sequences(x_train, maxlen=512)
	# 		x_test = sequence.pad_sequences(x_test, maxlen=512)

	# 		# Return either test or training set.
	# 		if test_set:
	# 			data_x, data_y = x_test[:num_elem], y_test[:num_elem]
	# 		else:
	# 			data_x, data_y = x_train[:num_elem], y_train[:num_elem]

	# 		# Look-up table translating words to integer id's.
	# 		word_to_id = reuters.get_word_index()
	# 		word_to_id["<PAD>"] = 0
	# 		word_to_id["<START>"] = 1
	# 		word_to_id["<UNK>"] = 2

	# 		# Inverse look-up table translating id's to words.
	# 		id_to_word = {v: k for k, v in word_to_id.items()}

	# 		# Name of each label in the dataset ordered by label id.
	# 		labels = [str(i) for i in range (np.max(y_train) + 1)]

	# 	# Return a dictionary with the data.
	# 	return {
	# 		'data_x': data_x,
	# 		'data_y': data_y,
	# 		'word_to_id': word_to_id,
	# 		'id_to_word': id_to_word,
	# 		'labels': labels
	# 	}
