import numpy as np

import sys
import random
import json

from sklearn.manifold import TSNE

from time import gmtime, strftime

from webserver import create_connection
from model_interface import ModelInterface
from dataset import Dataset
from label_histogram import LabelHistogram
from activation import Activation
from hidden_states import HiddenStates

"""
	This class controls the visualization backend. Computing heatmaps, histograms and projections and sending it to the front-end.
"""
class DeepVis:
	def __init__(self, model_folder):
		self.__folder = model_folder

		# Model interface to handle model operations.
		print('Loading model...')
		self.__model = ModelInterface(model_folder)

		# Dataset object to handle training and test set operations.
		print('Loading dataset...')
		self.__dataset = Dataset(model_folder)

		# Load model's weights
		print('Loading weights...')
		self.__model.load_epoch_weights(0)

		# Create a matrix with all the label histograms of the model.
		print('Calculating label histograms...')
		self.__label_histograms = LabelHistogram(self.__model, self.__dataset)

		# Create a matrix with all the hidden states of the model.
		print('Calculating hidden states...')
		self.__hidden_states = HiddenStates(self.__model, self.__dataset, elem_per_label=20)

		# Activation dispersion class.
		print('Calculating activation dispersion...')
		self.__actv_dispersion = Activation(self.__model, self.__dataset)

		# # data_x, data_y = self.__dataset.balanced_dataset(elem_per_label=200)
		# data_x, data_y = self.__dataset.get_elements(n=5000)
		# print(data_x.shape, data_y.shape)
		# self.__model.evaluate(data_x, data_y.reshape((data_y.shape[0],)))
		# print(self.__model.predict(data_x).shape)
		# print(data_y.shape)

		# hist = LabelHistogram(self.__model, self.__dataset)
		# print(hist.histograms()[-101:-1])
		# print(hist.unit_order()[-101:-1])
		# hist.sort_histograms(4, label=1, method='lle')
		# print(hist.histograms()[-101:-1])
		# print(hist.unit_order()[-101:-1])

		# p = hist.project_histograms(layer=None, method='t-sne')
		# print(p)

		# d = hist.get_data(layer=7, projection_method='t-sne')
		# print(json.dumps(d))

		# actv = Activation(self.__model, self.__dataset)
		# s = actv.activation_dispersion(4, 45, label=None, elem_per_label=20)
		# print(s)

		# hs = HiddenStates(self.__model, self.__dataset, elem_per_label=20)
		# obj = hs.get_data(layer=3, clusters=500, projection_method='t-sne')
		# print(sys.getsizeof(obj))

		# print('Request intra layer projection...')
		# intralayer_projection = self.intra_layer_projection(4)

		# print('Request inter layer projection...')
		# interlayer_projection = self.inter_layer_projection()

		# print('Request hidden state projection...')
		# hiddenstate_projection = self.hidden_state_projection(3)

		# print('Request activation dispersion...')
		# activation_dispersion = self.activation_dispersion(5, 0)

		# Open front-end connection
		create_connection(self)

	# Returns a JSON string with basic info about each layer of the model.
	def model_info(self):
		model_info = {
			'id': 'model_info',
			'layers': [{
				'layer_type': self.__model.layer_type(layer_index=i),
				'layer_units': self.__model.layer_size(layer_index=i),
				'input_shape': [int(x) for x in self.__model.layer_input_shape(layer_index=i)],
				'output_shape': [int(x) for x in self.__model.layer_output_shape(layer_index=i)],
				'activation_function': self.__model.layer_activation_function(layer_index=i)
			} for i in range(self.__model.number_of_layers())],
			'n_labels': self.__dataset.total_labels()
		}

		return json.dumps(model_info)

	# Returns the label histograms of a single layer plus its projections.
	def intra_layer_projection(self, layer, sorting, projection):
		msg = self.__label_histograms.get_data(layer=layer, sorting=sorting, projection=projection)
		return json.dumps(msg)

	# Returns the label histograms of the whole model plus its projections.
	def inter_layer_projection(self, projection):
		msg = self.__label_histograms.get_data(layer=None, projection=projection)
		return json.dumps(msg)

	# Returns the hidden states produced by a LSTM layer plus its projections (clusterized)
	def hidden_state_projection(self, layer, projection):
		msg = self.__hidden_states.get_data(layer=layer, projection=projection)
		return json.dumps(msg)

	# Returns the activation dispersion of a single unit in the model.
	def activation_dispersion(self, layer, unit):
		msg = self.__actv_dispersion.activation_dispersion(layer=layer, unit=unit)
		return json.dumps(msg)

	# Returns the activation scatterplots of two units.
	def activation_scatterplot(self, layer1, unit1, layer2, unit2):
		msg1 = self.__actv_dispersion.activation_dispersion(layer=layer1, unit=unit1)
		msg2 = self.__actv_dispersion.activation_dispersion(layer=layer2, unit=unit2)

		msg = {}
		msg['id'] = 'ActivationScatterplot'
		msg['unit1'] = msg1
		msg['unit2'] = msg2

		return json.dumps(msg)

if __name__ == '__main__':
	# Set random seed for reproducibility.
	np.random.seed(0)
	random.seed(0)

	# Get folder from command line.
	folder = str(sys.argv[1])

	# Send the model data to the mlvis library.
	vis = DeepVis(folder)