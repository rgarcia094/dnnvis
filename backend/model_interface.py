import json
import sys
import os

from keras.models import Sequential, model_from_json
from keras.utils import np_utils
"""
	Interface class to perform model analysis. Eventually may handle both keras and tensorflow models.

	Explanation about different layers:
		* Embedding:
			- input shape = (batch size, number of elements in the input).
			- weights = (total number of possible elements, output dimensionality of each element): 
			- output shape = (batch size, number of elements in the input, output dimensionality of each element). 

			Instead of applying matrix multiplication to calculate the output, the embedding layer works as a look-up table to find the embedding
			representation of each element. For example, if a input has value 50, the embedding layer will simply output the 50th row of its weight 
			matrix. The Embedding layer only has one weight matrix with shape (MAX_ELEMENT_INDEX, OUTPUT_DIMENSION) and has no bias.

		* Convolutional 1D:
			- input shape = (batch size, number of elements in the input, element input dimensionality)
			- weights = (kernel size, element input dimensionality, number of filters)
			- bias = (number of filters)
			- output shape = (batch size, number of elements in the output, number of filters) 

			For each filter in the layer, the Conv1D operates at each sequence of kernel adjancent elements in the input, multiplying all their 
			values by (kernel size, element input dimensionality). If padding is not same, then number of elements in the output will be smaller
			than in the input.

		* LSTM:
			- input shape = (batch size, number of elements in the input, element dimensionality)
			- weights 1 = (element dimensionality, 4*number of units)
			- weights 2 = (number of units, 4*number of units)
			- bias = (4*number of units)
			- output shape = (batch size, number of units)

			The matrix WEIGHTS 1 has the weights that will be applied to each input element at each time step. As each LSTM unit has 4 operations,
			there are 4*element dimensionality weights in each unit. The matrix WEIGHT 2 has the weights that will be applied to the recurrent
			state of the network, i.e., to the N outputs produced by the previous time step, where N is the number of units.

	TODO:
		* Create a function to calculate the hidden state of a LSTM cell (i.e. activations) in a given time step of a given epoch.
		* Verify if LSTM weights are being retrived correctly (i.e. the for indexes are from the same unit.)
"""
class ModelInterface:
	# Initialize the interface with the model architecture and the number of epochs the model was trained.
	def __init__(self, model_folder):
		self.__folder = model_folder

		# Load model architecture.
		self.__model = self.__load_model_architecture()

		# The number of epochs the model was trained is equal to the number of .hdf5 files the folder has.
		self.__n_epochs = len([file for file in os.listdir(model_folder) if file.endswith('hdf5')])

		print('Model loaded...')
		print('Model', self.__model)
		print('Folder', self.__folder)
		print('Epochs', self.__n_epochs)		

	# Get the number of current stored epochs.
	def number_of_epochs(self):
		return self.__n_epochs

	# Load the model architecture from JSON file, returning it.
	def __load_model_architecture(self):
		try:
			with open(self.__folder + '/model.json') as json_model:
				json_string = json.load(json_model)
				return model_from_json(json_string)
		except FileNotFoundError:
			sys.exit('Model not found.')

	# Reload the model architecture from JSON file.
	def reset_model_architecture(self):
		self.__model = self.__load_model_architecture()

	# Set weights for a given epoch.
	def load_epoch_weights(self, epoch):
		self.__model.load_weights(self.__folder + '/weights_epoch' + str(epoch) + '.hdf5', by_name=False)

	# # Get the weights for a given epoch.
	# def get_weights(self, epoch=None):
	# 	# If epoch is not provided, use the last one.
	# 	if epoch is not None:
	# 		self.set_weights(epoch)

	# 	return self.__model.get_weights()

	# # Get the weights in a particular layer for a given epoch.
	# def get_layer_weights(self, layer_index, epoch=None):
	# 	# If epoch is not provided, use the current one.
	# 	if epoch is not None:
	# 		self.set_weights(epoch)

	# 	return self.__model.layers[layer_index].get_weights()

	# """ Get the weights in a particular unit for a given epoch.
	# 		- Embedding layers: Return an array with the values of one output dimension for all possible elements.
	# 		- Dense layers: Return an array with the weights for every input dimension.
	# 		- Convolutional layers: Return a matrix with the weights for every input feature and kernel (kernel, element feature).
	# 		- LSTM layers: Return a matrix with the weights for each of the 4 LSTM components for all element featuers. (element features, 4).
	# 		- TODO: Implement for Conv2D and LSTM with sequential output.
	# """
	# def get_unit_weights(self, layer_index, unit_index, epoch=None):
	# 	# If epoch is not provided, use the current one.
	# 	if epoch is not None:
	# 		self.set_weights(epoch)

	# 	# Different layers have different weight matrix layouts.
	# 	layer_type = self.layer_type(layer_index)

	# 	if layer_type == 'Embedding' or layer_type == 'Dense':
	# 		return self.__model.layers[layer_index].get_weights()[0][:, unit_index]
	# 	elif layer_type == 'Conv1D':
	# 		return self.__model.layers[layer_index].get_weights()[0][:, :, unit_index]
	# 	elif layer_type == 'LSTM':
	# 		return self.__model.layers[layer_index].get_weights()[0][:, 4*unit_index:4*unit_index+4]
	# 	else:
	# 		return None

	# # Get the weights applied to the recurrent hidden state of a LSTM. Return None if used to other layers
	# def get_hidden_state_weights(self, layer_index, unit_index, epoch=None):
	# 	# Only valid with LSTM.
	# 	if self.layer_type(layer_index) != 'LSTM':
	# 		return None

	# 	# If epoch is not provided, use the current one.
	# 	if epoch is not None:
	# 		self.set_weights(epoch)

	# 	return self.__model.layers[layer_index].get_weights()[1][:, 4*unit_index:4*unit_index+4]

	# Evaluate the model.
	def evaluate(self, data_x, data_y):
		self.__model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
		scores = self.__model.evaluate(data_x, np_utils.to_categorical(data_y, 10), verbose=1)
		# self.__model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
		# scores = self.__model.evaluate(data_x, data_y, verbose=1)
		print("Accuracy: %.2f%%" % (scores[1]*100))

	# Predict labels for a given dataset.
	def predict(self, input_data):
		return self.__model.predict(input_data)

	# Get the number of layers in the model.
	def number_of_layers(self):
		return len(self.__model.layers)

	# Get the type of a particular layer.
	def layer_type(self, layer_index):
		return self.__model.get_config()[layer_index]['class_name']

	# Get the number of units (or filters for conv layers) in the model.
	def model_units(self):
		units = 0

		for layer in range(len(self.__model.layers)):
			units += self.layer_size(layer_index=layer)

		return units

	# Get the number of units (or filters for conv layers) in a particular layer.
	def layer_size(self, layer_index):
		# Check if the layer has units (for recurrent and dense layers),
		if 'units' in self.__model.layers[layer_index].get_config():
			return self.__model.layers[layer_index].get_config()['units']
		# Check if the layer has filters (for convolutional layers).
		elif 'filters' in self.__model.layers[layer_index].get_config():
			return self.__model.layers[layer_index].get_config()['filters']
		# Number of units in a Embedding layer is equal to the output
		elif self.layer_type(layer_index) == 'Embedding':
			return self.__model.layers[layer_index].get_config()['output_dim']
		# Other kind of layers have no units or filters.
		else:
			return 0

	# # Check if a particular layer use bias.
	# def check_layer_bias(self, layer_index):
	# 	if 'use_bias' in self.__model.layers[layer_index].get_config():
	# 		return self.__model.layers[layer_index].get_config()['use_bias']
	# 	else:
	# 		return False

	# # Get the bias vector in a particular layer.
	# def get_layer_bias(self, layer_index, epoch=None):
	# 	# If epoch is not provided, use the current one.
	# 	if epoch is not None:
	# 		self.set_weights(epoch)

	# 	if self.check_layer_bias(layer_index):
	# 		# LSTMs have an additional matrix, so the bias is the third one.
	# 		if self.layer_type(layer_index) == 'LSTM':
	# 			return self.__model.layers[layer_index].get_weights()[2]
	# 		else:
	# 			return self.__model.layers[layer_index].get_weights()[1]
	# 	else:
	# 		return None

	# # Get the bias in a particular unit. LSTM layers have 4 bias values in a single unit. Others layers have only one.
	# def get_unit_bias(self, layer_index, unit_index, epoch=None):
	# 	if self.check_layer_bias(layer_index):
	# 		# If is a LSTM unit, return a vector with 4 bias values.
	# 		if self.layer_type(layer_index) == 'LSTM':
	# 			return self.get_layer_bias(layer_index, epoch)[4*unit_index:4*unit_index+4]
	# 		# For other layers, return a single value.
	# 		else:
	# 			return self.get_layer_bias(layer_index, epoch)[unit_index]
	# 	else:
	# 		return None

	# Get the input shape of a particular layer.
	def layer_input_shape(self, layer_index):
		return self.__model.layers[layer_index].input_shape[1:]

	# Get the output shape of a particular layer.
	def layer_output_shape(self, layer_index):
		return self.__model.layers[layer_index].output_shape[1:]

	# Return the first dimension of the layer's kernel.
	def kernel_size(self, layer_index):
		return self.__model.layers[layer_index].get_config()['kernel_size'][0]

	# Return the padding technique of the layer.
	def padding(self, layer_index):
		return self.__model.layers[layer_index].get_config()['padding']

	def pool_size(self, layer_index):
		return self.__model.layers[layer_index].get_config()['pool_size'][0]

	# Get the activation function of a particular layer.
	def layer_activation_function(self, layer_index):
		if 'activation' in self.__model.layers[layer_index].get_config():
			return self.__model.layers[layer_index].get_config()['activation']
		else:
			return None

	# Check if a LSTM layer returns a sequential output or not.
	def return_sequences(self, layer_index):
		if 'return_sequences' in self.__model.layers[layer_index].get_config():
			return self.__model.layers[layer_index].get_config()['return_sequences']
		else:
			return False

	# Evaluate the activations produced by one of the model's layers for a given input data.
	def layer_activations(self, layer_index, input_data):
		# Input layer doesn't do anything, return input data.
		if (self.layer_type(layer_index) == 'InputLayer'):
			return input_data

		# Create an auxiliar model containing only the desired layer.
		aux_model = Sequential()
		aux_model.add(self.__model.layers[layer_index])

		# Predict and return the activations produced by the layer.
		activations = aux_model.predict(input_data)
		return activations

	# # Get the activations the model produces for a given input set passed as parameter.
	# def get_activations_for_data(self, input_data):
	# 	# List where we will store the activations on each layer
	# 	layers_activations = []

	# 	# Calculate activations on each layer.
	# 	for layer_index in range(self.get_number_of_layers()):
	# 		# Ignore input layer as it's not allowed to have a model only with an input layer.
	# 		if (self.layer_type(layer_index) != 'InputLayer'):
	# 			# Create a model containing only the current layer.
	# 			aux_model = Sequential()
	# 			aux_model.add(self.__model.layers[layer_index])

	# 			# Calculate the layer activations
	# 			activations = aux_model.predict(input_data)
	# 			layers_activations.append(activations)

	# 			# Update layer input data
	# 			input_data = activations

	# 	print(layers_activations[-1])

	# 	return layers_activations


	# # Generate a partial model containing the same layers of self.__model until layer_index
	# def create_partial_model(self, layer_index):
	# 	partial_model = Sequential()

	# 	for i in range(0, layer_index + 1):
	# 		partial_model.add(self.__model.layers[i])

	# 	return ModelInterface(partial_model, self.__param_configs)

	# # Print layer configuration for debugging purposes.
	# def print_layer_config(self, layer_index):
	# 	print(self.__model.layers[layer_index].get_config())