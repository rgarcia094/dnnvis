import numpy as np

from model_interface import ModelInterface
from dataset import Dataset

"""
	Builds a scatterplot with the correlation between activation of two units in the model using a balanced dataset.
"""
class Activation:
	def __init__(self, model, dataset, epoch=0):
		self.__model = model
		self.__dataset = dataset
		self.__epoch = epoch

	# Build the scatterplot for the units passed as parameters.
	def activation_dispersion(self, layer, unit, label=None, elem_per_label=100, test_set=True):
		# Generate histograms using a balanced dataset with the same number of elements for each label.
		if label == None:
			data_x, data_y = self.__dataset.balanced_dataset(elem_per_label=elem_per_label, test_set=test_set)
		
		# Use only elements from the chosen layer in the dataset.
		else:
			data_x, data_y = self.__dataset.get_elements_from_label(label, n=elem_per_label, test_set=test_set)

		# Input data has to be updated on each layer, as it's the output of the previous one.
		input_data = data_x

		# Iterate over the layers to calculate activations on the desired layers.
		for l in range(self.__model.number_of_layers()):
			# Calculate activations for the current layer. Input layers return no activations.
			if self.__model.layer_type(l) != 'InputLayer':
				activations = self.__model.layer_activations(l, input_data)
			else:
				activations = input_data

			# If that is the desired layer, store the produced activations.
			if l == layer:
				# Conv2D layers have matrix outputs. Get the mean activation.
				if self.__model.layer_type(l) == 'Conv2D':
					a = activations[:, :, :, unit]
					a = np.sum(a, axis=(1, 2))
				elif self.__model.layer_type(l) == 'Conv1D':
					a = activations[:, :, unit]
					a = np.sum(a, axis=1)
				elif self.__model.layer_type(l) == 'LSTM' and self.__model.return_sequences(l):
					a = activations[:, :, unit]
					a = np.sum(a, axis=1)
				elif self.__model.layer_type(l) == 'Embedding':
					a = activations[:, :, unit]
					a = np.sum(a, axis=1)
				else:
					a = activations[:, unit]

			# If last layer, store the predicted label.
			if l == self.__model.number_of_layers()-1:
				data_p = activations

			# Update input data for the next layer.
			input_data = activations

		actual_labels = data_y[:, 0]

		# Transform one-hot encodes in numeric labels.
		if self.__dataset.total_labels() > 2:
			predicted_labels = np.argmax(data_p, axis=1)

		# Round results to label when binary classification.
		else:
			predicted_labels = np.round(data_p)[:, 0].astype(int)


		# Reset model, otherwise it may affect next operations.
		self.__model.reset_model_architecture()
		self.__model.load_epoch_weights(self.__epoch)

		# Return data in a dictionary allowing JSON encoding.
		d = {
			'id': 'ActivationDispersion',
			'layer': layer,
			'unit': unit,
			'activations': a.tolist(),
			'actual_labels': actual_labels.tolist(),
			'predicted_labels': predicted_labels.tolist()
		}

		return d