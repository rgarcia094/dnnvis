import numpy as np

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from matplotlib.colorbar import ColorbarBase

from keras.datasets import cifar10
from keras.models import Sequential, Model, load_model
from keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Dense
from keras.utils import np_utils
from keras.optimizers import SGD

from sklearn.manifold import TSNE
from sklearn.cluster import KMeans, AffinityPropagation, MeanShift, DBSCAN

from mpl_toolkits.axes_grid1 import make_axes_locatable

from scipy.stats import entropy
from scipy.stats import mode

# Calculate activation vectors in the l-th layer.
def layer_activation(model, l, dataset):
	p_model = Sequential()
	
	for i in range(l+1):
		p_model.add(model.layers[i])
	
	activations = p_model.predict(dataset)
	
	return activations

def layer_activation2(model, l, dataset):
	p_model = Model(model.input, model.layers[l].output)
	activations = p_model.predict(dataset)

	return activations

# Build class histograms of a dense layer's units.
def class_histogram_dense(model, layer, x_train, t_labels, p_labels, num_classes):
	num_units = model.layers[layer].output_shape[-1]
	
	# Build the histogram.
	histograms = np.zeros((num_units, num_classes))
	
	# Only consider activations produced by correctly classified elements.
	x_train = x_train[t_labels == p_labels]
	t_labels = t_labels[t_labels == p_labels]
	
	# Calculate activation of observations for every c class.
	for c in range(num_classes):
		# Activations produced by elements of class c.
		a = layer_activation(model, layer, x_train[t_labels == c])
		
		# Calculate the size of c-th bin for every unit.
		for u in range(num_units):
			b = a[:, u]
			histograms[u, c] = b[b > 0.0].shape[0]
	
	return histograms

# Build matrix mapping training elements to kernels whose activate for them.
def activation_matrix_conv2d(model, layer, x_train, t_labels, p_labels, num_classes):
	# Matrix dimensions.
	num_units = model.layers[layer].output_shape[-1]
	num_elems = x_train.shape[0]
	
	# Build matrix.
	matrix = np.zeros((num_elems, num_units))
	
	# Calculate activation of elements from each c class.
	i = 0
	for c in range(num_classes):		
		# Activations produced by elements of class c.
		a = layer_activation(model, layer, x_train[t_labels == c])
		
		# Calculate the size of c-th bin for every unit.
		for u in range(num_units):
			# Fill the matrix with 1 if positive activation or 0 otherwise.
			b = np.max(a[:, :, :, u], axis=(1, 2))
			b = (b > 0.0).astype(int)
			matrix[i:i+b.shape[0], u] = b
			
		i += b.shape[0]
	
	return matrix

# Returns the number of training examples which do not activate for any kernel.
def unactivated_elements_size(matrix):
	m = np.max(matrix, axis=1)
	return m[m == 0.0].shape[0]

# Build class histograms. Each bin represents the number of images which activates for that kernel.
def class_histogram_conv2d_v1(model, layer, x_train, t_labels, p_labels, num_classes):
	num_units = model.layers[layer].output_shape[-1]
		
	# Build the histogram.
	histograms = np.zeros((num_units, num_classes))
	selectivity = np.zeros((num_units, num_classes))
	
	# Only consider activations produced by correctly classified elements.
	x_train = x_train[t_labels == p_labels]
	t_labels = t_labels[t_labels == p_labels]

	# Calculate activation values for each group of n samples.
	n = 100
	for i in range(0, x_train.shape[0], n):
		# Activations produced by the current group.
		actvs = layer_activation(model, layer, x_train[i:i+n])
		labels = t_labels[i:i+n]

		# Fill the histogram matrix with each class x unit value.
		for c in range(num_classes):
			# Activations of all elements from class c in the n-group.
			a = actvs[labels == c]
			
			for u in range(num_units):
				# Max activation produced by each element in any position of the kernel u.
				b = np.max(a[:, :, :, u], axis=(1, 2))

				histograms[u, c]  += b[b > 0.0].shape[0]
				selectivity[u, c] += b[b > 0.0].shape[0] - b[b == 0.0].shape[0]

	# Updates selectivity value as well.
	for c in range(num_classes):
		histograms[:, c] = histograms[:, c] / t_labels[t_labels == c].shape[0]
		selectivity[:, c] = selectivity[:, c] / t_labels[t_labels == c].shape[0]

	return histograms, selectivity

# Build class histograms, measuring the number of positive pixels each class produced.
def class_histogram_conv2d_v2(model, layer, x_train, t_labels, p_labels, num_classes):
	num_units = model.layers[layer].output_shape[-1]
	
	# Build the histogram.
	histograms = np.zeros((num_units, num_classes))
	selectivity = np.zeros((num_units, num_classes))
	
	# Only consider activations produced by correctly classified elements.
	x_train = x_train[t_labels == p_labels]
	t_labels = t_labels[t_labels == p_labels]
	
	# Calculate activations of observation pixels for every c class.
	for c in range(num_classes):
		# Activations produced by elements of class c.
		a = layer_activation(model, layer, x_train[t_labels == c])
		
		# Calculate the size of c-th bin for every unit.
		for u in range(num_units):
			# We want the number of individual pixels with positive activations.
			b = a[:, :, :, u].reshape((a.shape[0]*a.shape[1]*a.shape[2],))
			histograms[u, c] = b[b > 0.0].shape[0] - b[b == 0.0].shape[0]
			
			# Compute the selectivity of the kernel.
			selectivity[u, c] = histograms[u, c] / b.shape[0]
			
	return histograms, selectivity

def class_histogram_conv2d_v4(model, layer, x_train, t_labels, p_labels, num_classes): # layer_activation2
	width = model.layers[layer].output_shape[1]
	height = model.layers[layer].output_shape[2]
	units = model.layers[layer].output_shape[3]
	
	# Build the histogram.
	histograms = np.zeros((units, width, height, num_classes))
	selectivity = np.zeros((units, width, height, num_classes))
	
	# Only consider activations produced by correctly classified elements.
	x_train = x_train[t_labels == p_labels]
	t_labels = t_labels[t_labels == p_labels]
	
	# Calculate how many observations from class c activate positive values for each pixel in the layer's output.
	for c in range(num_classes):
		# Activations produced by elements of class c.
		a = layer_activation2(model, layer, x_train[t_labels == c])
		
		b = (a > 0.0).astype(int)
		b = np.sum(b, axis=0)

		b = np.swapaxes(b, 1, 2)
		b = np.swapaxes(b, 0, 1)

		histograms[:, :, :, c] = b / a.shape[0]
		selectivity[:, :, :, c] = (b - (a.shape[0] - b)) / a.shape[0]
		
	return histograms, selectivity

# Build class histograms for every pixel in the output of a conv2d layer.
def class_histogram_conv2d_v3(model, layer, x_train, t_labels, p_labels, num_classes):
	width = model.layers[layer].output_shape[1]
	height = model.layers[layer].output_shape[2]
	units = model.layers[layer].output_shape[3]
	
	# Build the histogram.
	histograms = np.zeros((units, width, height, num_classes))
	selectivity = np.zeros((units, width, height, num_classes))
	
	# Only consider activations produced by correctly classified elements.
	x_train = x_train[t_labels == p_labels]
	t_labels = t_labels[t_labels == p_labels]
	
	# Calculate how many observations from class c activate positive values for each pixel in the layer's output.
	for c in range(num_classes):
		# Activations produced by elements of class c.
		a = layer_activation(model, layer, x_train[t_labels == c])
		
		# # Calculate the size of c-th bin for every pixel in the layer's output.
		# for u in range(units):
		# 	for h in range(height):
		# 		for w in range(width):
		# 			b = a[:, w, h, u]
		# 			histograms[u, w, h, c] = b[b > 0.0].shape[0] / b.shape[0]
		# 			selectivity[u, w, h, c] = (b[b > 0.0].shape[0] - b[b == 0.0].shape[0]) / b.shape[0]

		b = (a > 0.0).astype(int)
		b = np.sum(b, axis=0)

		b = np.swapaxes(b, 1, 2)
		b = np.swapaxes(b, 0, 1)

		histograms[:, :, :, c] = b / a.shape[0]
		selectivity[:, :, :, c] = (b - (a.shape[0] - b)) / a.shape[0]
		
	return histograms, selectivity

# Create an array with the kernel's id of each class histogram in a matrix.
def kernel_id_array(histograms):
	if len(histograms.shape) == 3:
		id_array = np.array([i for i in range(histograms.shape[0]) for _ in range(histograms.shape[1])])
	else:
		id_array = np.array([i for i in range(histograms.shape[0])])

	return id_array

# Merge all pixels of each kernel.
def merge_pixels_dim(histograms):
	return histograms.reshape((histograms.shape[0], histograms.shape[1]*histograms.shape[2], histograms.shape[3]))

# Calculate the selectivity of each position of all kernels in the layer. Type may be entropy or average difference.
def position_selectivity(h):
	ps = np.zeros((h.shape[0], h.shape[1]))

	for i in range(h.shape[0]):
		for j in range(h.shape[1]):
			avg_l = []

			for c1 in range(h.shape[2]):
				for c2 in range(c1+1, h.shape[2]):
					avg_l.append(h[i, j, c1] - h[i, j, c2])

			ps[i, j] = np.mean(np.abs(avg_l))

	return ps

# Calculate the class selectivity of each position in each kernel.
def position_class_selectivity(h):
	ps = np.zeros(h.shape)

	for k in range(h.shape[0]):
		for p in range(h.shape[1]):
			for c in range(h.shape[2]):
				diff = h[k, p, c] - h[k, p, :]
				diff = np.delete(diff, c)
				ps[k, p, c] = np.mean(diff)

	return ps

# Calculate an overall selectivity for each kernel.
def kernel_selectivitity(h, ps, sel_type):
	if sel_type == 'max':
		return np.max(ps, axis=1)
	elif sel_type == 'mean':
		return np.mean(ps, axis=1)
	elif sel_type == 'median':
		return np.median(ps, axis=1)
	elif sel_type == 'pond':
		ks = np.zeros((h.shape[0],))

		for k in range(h.shape[0]):
			for p in range(h.shape[1]):
				max_c = np.max(h[k, p, :])
				u, c = np.unique(h[k, p, :], return_counts=True)
				nc = dict(zip(u, c))[max_c]

				ks[k] += ps[k, p] / nc

		ks /= h.shape[0]

		return ks
	else:
		return None


# Get the selectivity intensity of each kernel.
def selectivity_intensity(selectivity):
	# Calculate selectivity intensity per position.
	if len(selectivity.shape) == 3:
		# Max and min bin selectivity in each position.
		max_s = np.max(selectivity, axis=2)
		min_s = np.min(selectivity, axis=2)

		# Selectivity intensity of each position.
		s = max_s - min_s

		# Each kernel is represented by the max/mean SI for any position.
		return np.max(s, axis=1), np.mean(s, axis=1)

	# Calculate selectivity intensity per kernel.
	else:
		# Max and min bin selectivity in each position.
		max_s = np.max(selectivity, axis=1)
		min_s = np.min(selectivity, axis=1)

		return max_s - min_s

# Cluster the histograms in each kernel in k groups using k-means.
def cluster_histograms(histograms, selectivity, k):
	h = np.zeros((histograms.shape[0], k, histograms.shape[2]))
	s = np.zeros((histograms.shape[0], k, histograms.shape[2]))

	for i in range(histograms.shape[0]):
		kmeans = KMeans(n_clusters=k).fit(histograms[i, :, :]) 
		h[i, :, :] = kmeans.cluster_centers_

		for j in range(k):
			if selectivity[i, kmeans.labels_ == j, :].shape[0] > 0:
				s[i, j, :] = np.mean(selectivity[i, kmeans.labels_ == j, :], axis=0)
			else:
				s[i, j, :] = 0.0

	return h, s

# Merge all kernels' class histograms in a single matrix.
def merge_array_dim(histograms):
	return histograms.reshape((histograms.shape[0]*histograms.shape[1], histograms.shape[2]))

# Remove class histograms whose selectivity is positive for more than 50% of classes or those which selectivity is not positive for any class.
def remove_histograms_by_selectivity(t1, t2, histograms, selectivity, actv_matrix, id_array, num_classes):
	t1 = np.max(selectivity, axis=1) > t1
	t2 = np.sum((selectivity > 0.0).astype(int), axis=1)/num_classes <= t2

	s = selectivity[t1*t2, :]
	h = histograms[t1*t2, :]
	k = id_array[t1*t2]
	m = actv_matrix[:, np.unique(k)]

	return h, s, m, k

# Project a set of class histograms to bidimensional space using t-SNE.
def project_histogram(histograms, p=30, e=12.0, l=200.0):
	tsne = TSNE(perplexity=p, early_exaggeration=e, learning_rate=l, verbose=1).fit(histograms)
	projection = tsne.embedding_
	
	return projection, tsne.kl_divergence_

# Sort histograms using hierarchical clustering.
def sort_histograms(histograms, selectivity, id_array, projection):
	clustering = AgglomerativeClustering(n_clusters=histograms.shape[0], compute_full_tree=True).fit(histograms)

	merge_order = clustering.children_
	index_list = [[i] for i in range(histograms.shape[0])]

	for i, j in merge_order:
		index_list.append(index_list[i] + index_list[j])

	order = index_list[-1]

	histograms = histograms[order, :]
	selectivity = selectivity[order, :]
	id_array = id_array[order]
	projection = projection[order]

	return histograms, selectivity, id_array, projection

# Sort the bins of each histogram in the dataset.
def sort_bins(histograms, selectivity):
	h = np.zeros(histograms.shape)
	s = np.zeros(selectivity.shape)
	o = np.zeros(histograms.shape)

	for i in range(histograms.shape[0]):
		order = np.argsort(histograms[i, :])[::-1]

		h[i, :] = histograms[i, order]
		s[i, :] = selectivity[i, order]
		o[i, :] = order

	return h, s, o

# Return how many observations activated in each unit.
def layer_activation_number(actv_matrix, t_labels, p_labels, num_classes):
	# Number of units.
	actv_amount = np.zeros((actv_matrix.shape[1]))
	
	# Calculate the number of activations per pixels for images of each class.
	i = 0
	for c in range(num_classes):
		# Get the labels true and predicted from c class.
		t = t_labels[t_labels == c]
		p = p_labels[t_labels == c]
		
		# Get the submatrix containing only correctly classified elements from c class.
		m = actv_matrix[i:i+t.shape[0], :][t == p]
		
		# Increase the number of elements according to matrix.
		actv_amount = actv_amount + np.sum(m, axis=0)
		
		# Update index.
		i += t.shape[0]
		
	return actv_amount

# Check if the selectivity vector of each kernel respects both thresholds.
def check_selectivity_threshold(selectivity):
	selec_threshold = np.zeros((selectivity.shape[0],))
		
	for i in range(selectivity.shape[0]):
		# Check if the number of positive selectivity is no more than half the number of classes.
		perc_threshold = int(selectivity[i, selectivity[i, :] > 0.0].shape[0] <= selectivity[i, :].shape[0]/2)
		
		# Check if the max threshold is positive.
		max_s_threshold = int(np.max(selectivity[i, :]) > 0.0)
		
		# If one of the thresholds is not respected, returns 0. Otherwise 1.
		selec_threshold[i] = perc_threshold * max_s_threshold
	
	return selec_threshold

# Returns a vector containing the selectivity value of each kernel.
def get_kernel_selectivity_value(selectivity):
	# Get the max and min selectivity value of each kernel.
	selec_max = np.max(selectivity, axis=1)
	selec_min = np.min(selectivity, axis=1)
	
	# Returns the difference between max and min value.
	return selec_max - selec_min

# Plot the class histogram projection using pie-charts to represent each unit/datapoint.
def plot_projection_piechart(projection, histograms, n, labels, l_type, offset, size):
	f, ax = plt.subplots(figsize=(15, 10))

	# Transform histogram data into pie chart.
	h = np.zeros((histograms.shape[0], n))
	c = np.zeros((histograms.shape[0], n)).astype(int)
	
	for i in range(h.shape[0]):
		c[i, :] = np.argsort(histograms[i, :])[::-1][:n]
		h[i, :] = histograms[i, c[i, :]]
		h[i, :] = np.maximum(h[i, :], np.zeros((h.shape[1],)))
		
		s = np.sum(h[i, :])
		if s > 0:
			h[i, :] /= s
		
		for j in range(1, h.shape[1]):
			h[i, j] += h[i, j-1]

	# Plot piecharts.
	for i in range(h.shape[0]):
		for j in range(h.shape[1]):
			if j == 0:
				a = np.pi/2
			else:
				a = np.pi/2 + 2*np.pi*h[i, j-1]

			b = np.pi/2 + 2*np.pi*h[i, j]

			x = [0] + np.cos(np.linspace(a, b, 10)).tolist()
			y = [0] + np.sin(np.linspace(a, b, 10)).tolist()
			xy = np.column_stack([x, y])

			ax.scatter(projection[i, 0], projection[i, 1], marker=(xy, 0), s=size, c=cm.tab10(c[i, j]))
		
	# Plot unit indexes.
	if offset > 0.0:
		for i, p in enumerate(projection):
			ax.annotate(str(i), (p[0] + offset, p[1] + offset))
	
	# Plot the colorbar.
	cax = make_axes_locatable(ax).append_axes("right", size="5%", pad=0.05)
	cbar = ColorbarBase(cax, cmap=cm.tab10, ticks=[i/10.0 + 0.05 for i in range(10)])	
	cbar.ax.set_yticklabels(labels)
	cbar.ax.set_ylabel("Class Labels", rotation=-90, va="bottom", fontsize=15)

	# Set titles.
	ax.set_title(l_type, fontsize=20, pad=10)

	# Remove ticks
	ax.set_xticks([])
	ax.set_yticks([])
	
	plt.show()

# Plot the class histogram projection coloring units (datapoints) by the histogram selectivity thresholds
# and total activations.
def plot_auxiliar_projection(projection, thresholds, activations, labels, offset):
	f, (ax1, ax2) = plt.subplots(ncols=2, figsize=(20, 5), gridspec_kw={"width_ratios": [1, 1]})
	plt.subplots_adjust(wspace=0.1)
	
	# Plot points on the selectivity thresholds scatterplot.
	im = ax1.scatter(projection[:, 0], projection[:, 1], c=thresholds, cmap=cm.RdBu, s=100, vmin=0.0, vmax=2.4)
	
	# Plot unit indexes.
	if offset > 0.0:
		for i, p in enumerate(projection):
			ax1.annotate(str(i), (p[0] + offset, p[1] + offset))
		
	# Plot the colorbar.
	cax1 = make_axes_locatable(ax1).append_axes("right", size="5%", pad=0.05)
	cbar = f.colorbar(im, cax=cax1)
		
	# Plot points on the activation scatterplot.
	im = ax2.scatter(projection[:, 0], projection[:, 1], c=activations, cmap=cm.Blues, s=100, vmin=0.0, vmax=5000)
	
	# Plot unit indexes.
	if offset > 0.0:
		for i, p in enumerate(projection):
			ax2.annotate(str(i), (p[0] + offset, p[1] + offset))
		
	# Plot the colorbar.
	cax2 = make_axes_locatable(ax2).append_axes("right", size="5%", pad=0.05)
	cbar = f.colorbar(im, cax=cax2)
	
	# Set titles.
	ax1.set_title('Selectivity Threshold', fontsize=20, pad=10)
	ax2.set_title('Total Activations', fontsize=20, pad=10)
	
	# Remove ticks
	ax1.set_xticks([])
	ax1.set_yticks([])
	ax2.set_xticks([])
	ax2.set_yticks([])

	plt.show()

# Plot one histogram with the selectivity distribution, another histogram with the activation number distribution
# and the heatmap matrix of each training element x kernel pair.
def plot_auxiliar_visualizations(selectivity_values, activations, actv_matrix):
	f, (ax1, ax2, ax3) = plt.subplots(ncols=3, figsize=(20, 5), gridspec_kw={"width_ratios": [1, 1, 1]})
	
	# Selectivity Distribution.
	ax1.hist(selectivity_values, 20)
	ax1.axis([0, np.max(selectivity_values), 0, selectivity_values.shape[0]])
	
	# Activation Distribution.
	ax2.hist(activations, 20)
	ax2.axis([0, np.max(activations), 0, activations.shape[0]])
	
	# Activation Matrix.
	ax3.imshow(actv_matrix, aspect='auto', cmap=cm.Blues)
	
	# Set titles.
	ax1.set_title('Selectivity Distribution', fontsize=20, pad=10)
	ax2.set_title('Per Kernel Activation Distribution', fontsize=20, pad=10)
	ax3.set_title('Image x Kernel Activation Matrix', fontsize=20, pad=10)
	
	# Set axis labels
	ax1.set_xlabel('Selectivity Value', fontsize=15)
	ax1.set_ylabel('Number of Kernels', fontsize=15)
	ax2.set_xlabel('Number of Activations', fontsize=15)
	ax2.set_ylabel('Number of Kernels', fontsize=15)
	ax3.set_xlabel('Kernel Indexes', fontsize=15)
	ax3.set_ylabel('Training Images', fontsize=15)
	
	plt.show()

# Plot a matrix mapping training elements to the kernels they activate.
def plot_activation_matrix(actv_matrix):
	f, (ax) = plt.subplots(ncols=1, gridspec_kw={"width_ratios": [1]})
	
	ax.imshow(actv_matrix, aspect='auto', cmap=cm.Blues, vmin=0.0, vmax=1.0)

	ax.set_title('Image x Kernel Activation Matrix', fontsize=20, pad=10)
	ax.set_xlabel('Kernel Indexes', fontsize=15)
	ax.set_ylabel('Training Images', fontsize=15)

	plt.show()

	return actv_matrix[np.max(actv_matrix, axis=1) == 0].shape[0]

# Reduce the size of a giver layer keeping only the kernels in the units array.
def reduce_model_units(model, layer, units):
	n = units.shape[0]

	n_model = Sequential()
	
	done = None

	for l in range(len(model.layers)):
		if type(model.layers[l]) != Conv2D and type(model.layers[l]) != Dense:		
			if type(model.layers[l]) == MaxPooling2D:
				n_model.add(MaxPooling2D(pool_size=(2, 2)))
				
				if done is not None:
					done = np.array(done)
					done[0] /= 2
					done[1] /= 2
					done = tuple(done)
				
			elif type(model.layers[l]) == Dropout:
				drop_rate = model.layers[l].get_config()['rate']				
				n_model.add(Dropout(drop_rate))
			else:
				n_model.add(Flatten())
		
		elif l == layer:			
			if type(model.layers[l]) == Conv2D and l == 0:
				kernel = model.layers[l].get_config()['kernel_size']
				input_shape = model.layers[l].input_shape[1:]
				n_model.add(Conv2D(n, kernel_size=kernel, padding='same', activation='relu', trainable=False, input_shape=input_shape))
				
				w = model.layers[l].get_weights()[0][:, :, :, units]
				b = model.layers[l].get_weights()[1][units]
				n_model.layers[l].set_weights([w, b])
			
			elif type(model.layers[l]) == Conv2D:
				kernel = model.layers[l].get_config()['kernel_size']
				n_model.add(Conv2D(n, kernel_size=kernel, padding='same', activation='relu', trainable=False))

				w = model.layers[l].get_weights()[0][:, :, :, units]
				b = model.layers[l].get_weights()[1][units]
				n_model.layers[l].set_weights([w, b])
				
			elif l == len(model.layers) - 1:
				n_model.add(Dense(n, activation='softmax', trainable=False))
			
				w = model.layers[l].get_weights()[0][:, units]
				b = model.layers[l].get_weights()[1][units]
				n_model.layers[l].set_weights([w, b])

			else:
				n_model.add(Dense(n, activation='relu', trainable=False))
				
				w = model.layers[l].get_weights()[0][:, units]
				b = model.layers[l].get_weights()[1][units]
				n_model.layers[l].set_weights([w, b])

			done = model.layers[l].output_shape[1:]
			
		elif done is not None:				
			if type(model.layers[l]) == Conv2D:
				n_filters = model.layers[l].get_config()['filters']
				kernel = model.layers[l].get_config()['kernel_size']
				n_model.add(Conv2D(n_filters, kernel_size=kernel, padding='same', activation='relu'))
				
				w = model.layers[l].get_weights()[0][:, :, units, :]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
				
			elif l == len(model.layers) - 1:
				n_units = model.layers[l].get_config()['units']
				n_model.add(Dense(n_units, activation='softmax'))
				
				w = model.layers[l].get_weights()[0][units, :]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
			
			else:
				n_units = model.layers[l].get_config()['units']
				n_model.add(Dense(n_units, activation='relu'))

				w = model.layers[l].get_weights()[0]
				w2 = n_model.layers[l].get_weights()[0]
				
				w = w.reshape(done + (w.shape[1],))[:, :, units, :].reshape(w2.shape)
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
							
			done = None
		
		elif l < layer:
			if type(model.layers[l]) == Conv2D and l == 0:
				n_filters = model.layers[l].get_config()['filters']
				kernel = model.layers[l].get_config()['kernel_size']
				input_shape = model.layers[l].input_shape[1:]
				n_model.add(Conv2D(n_filters, kernel_size=kernel, padding='same', activation='relu', trainable=False, input_shape=input_shape))
				
				w = model.layers[l].get_weights()[0]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
			
			elif type(model.layers[l]) == Conv2D:
				n_filters = model.layers[l].get_config()['filters']
				kernel = model.layers[l].get_config()['kernel_size']
				n_model.add(Conv2D(n_filters, kernel_size=kernel, padding='same', trainable=False, activation='relu'))
				
				w = model.layers[l].get_weights()[0]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
				
			elif l == len(model.layers) - 1:
				n_units = model.layers[l].get_config()['units']
				n_model.add(Dense(n_units, activation='softmax', trainable=False))
				
				w = model.layers[l].get_weights()[0]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
			
			else:
				n_units = model.layers[l].get_config()['units']
				n_model.add(Dense(n_units, activation='relu', trainable=False))
				
				w = model.layers[l].get_weights()[0]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
		else:
			if type(model.layers[l]) == Conv2D and l == 0:
				n_filters = model.layers[l].get_config()['filters']
				kernel = model.layers[l].get_config()['kernel_size']
				input_shape = model.layers[l].input_shape[1:]
				n_model.add(Conv2D(n_filters, kernel_size=kernel, padding='same', activation='relu', input_shape=input_shape))
				
				w = model.layers[l].get_weights()[0]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
			
			elif type(model.layers[l]) == Conv2D:
				n_filters = model.layers[l].get_config()['filters']
				kernel = model.layers[l].get_config()['kernel_size']
				n_model.add(Conv2D(n_filters, kernel_size=kernel, padding='same', activation='relu'))
				
				w = model.layers[l].get_weights()[0]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
				
			elif l == len(model.layers) - 1:
				n_units = model.layers[l].get_config()['units']
				n_model.add(Dense(n_units, activation='softmax'))
				
				w = model.layers[l].get_weights()[0]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])
			
			else:
				n_units = model.layers[l].get_config()['units']
				n_model.add(Dense(n_units, activation='relu'))
				
				w = model.layers[l].get_weights()[0]
				b = model.layers[l].get_weights()[1]
				n_model.layers[l].set_weights([w, b])

	# Compile the model.
	# opt = SGD(lr=0.01, momentum=0.9, decay=10e-6, nesterov=True)
	n_model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])

	return n_model