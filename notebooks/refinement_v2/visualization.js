data = null;
glob_kernel = null;
selected_classes = [];
highlight = "any_class";
selected_slice = null;

$.getJSON("layer5_chpk.json", function(json) {
	data = json
	console.log(data)

	// Update kernel selector.
	d3.select("#kernel-selector").selectAll("option")
		.data(data["id_array"].filter((v, i, a) => a.indexOf(v) === i).sort((a, b) => a - b))
	.enter().append("option")
		.attr("id", function(d) { return "kernel_" + d; })
		.attr("value", function(d) { return d; })
		.text(function(d) { return "Kernel " + d; });

	d3.select("#kernel-selector").append("option")
		.attr("id", "kernel_all")
		.attr("value", -1)
		.text("All Kernels");

	// Box to display histogram.
	var hist_xpos = 840;
	var hist_ypos = 70;
	var hist_width = 500;
	var hist_height = 300;

	var histogram_g = d3.select("#projection-svg").append("g")
		.attr("id", "histogram_view");

	histogram_g.append("rect")
		.attr("x", hist_xpos)
		.attr("y", hist_ypos)
		.attr("width", hist_width)
		.attr("height", hist_height)
		.style("fill", "white")
		.style("stroke", "black");

	// Display activation matrix.
	var activations = 0;
	for (var i = 0; i < data["actv_matrix"].length; i++) {
		activations += (1 - d3.max(data["actv_matrix"][i]));
	}

	d3.select("#projection-svg").append("text")
		.attr("id", "activation_text")
		.attr("x", 840)
		.attr("y", 400)
		.text("Number of Training Elements without Activation: " + activations)

	// Display projection
	var proj_xpos = 20;
	var proj_ypos = 70;
	var proj_size = 800;

	var projection_g = d3.select("#projection-svg").append("g")
		.attr("id", "projection_view");

	projection_g.append("rect")
		.attr("x", proj_xpos)
		.attr("y", proj_ypos)
		.attr("width", proj_size)
		.attr("height", proj_size)
		.style("fill", "white")
		.style("stroke", "black");

	var min_x = d3.min(data["projection"], function(d) { return d[0]; });
	var max_x = d3.max(data["projection"], function(d) { return d[0]; });
	var min_y = d3.min(data["projection"], function(d) { return d[1]; });
	var max_y = d3.max(data["projection"], function(d) { return d[1]; });

	var xScale = d3.scaleLinear()
		.domain([min_x, max_x])
		.range([25, proj_size-25]);

	var yScale = d3.scaleLinear()
		.domain([min_y, max_y])
		.range([25, proj_size-25]);

	var colorScale = d3.scaleOrdinal()
		.domain(d3.range(data["histograms"][0].length))
		.range(d3.schemePaired);

	var borderScale = d3.scaleLinear()
		.domain([0, 2])
		.range(["lightblue", "navy"]);

	// var pie = d3.pie()
	// 	.sort(null)
	// 	.value(function(d) { return d; });

	// var path = d3.arc()
	// 	.outerRadius(10)
	// 	.innerRadius(0);

	var rings = projection_g.selectAll(".projection_border")
		.data(data["projection"])
	.enter().append("circle")
		.attr("class", function (_, i) { return "projection_border kernel_" + data["id_array"][i]; })
		.attr("cx", function (d) { return proj_xpos + xScale(d[0]); })
		.attr("cy", function (d) { return proj_ypos + yScale(d[1]); })
		.attr("r", 10)
		.style("fill", function (_, i) { return borderScale(data["intensity_max"][data["id_array"][i]]); })
		.on("mouseover", function(_, i) {
			d3.select("#histogram_view").selectAll(".histogram_bin").remove("*");

			var n_labels = data["selectivity"][i].length

			var colorScale = d3.scaleOrdinal()
				.domain(d3.range(n_labels))
				.range(d3.schemePaired);

			var binScale = d3.scaleLinear()
				.domain([1, -1])
				.range([hist_ypos, hist_ypos + hist_height])

			var bins = histogram_g.selectAll(".histogram_bin")
				.data(data["selectivity"][i])
			.enter().append("rect")
				.attr("class", "histogram_bin")
				.attr("x", function(_, j) { 
					var pos = data["bin_order"][i][j];

					return hist_xpos + pos*hist_width/n_labels 
				})
				.attr("y", function(d, _) { 
					if (d > 0.0) {
						return binScale(d);
					} else {
						return binScale(0);
					}
				})
				.attr("width", hist_width/n_labels)
				.attr("height", function(d) { 
					if (d > 0.0) {
						return binScale(0) - binScale(d);
					} else {
						return binScale(d) - binScale(0);
					}
				})
				.style("fill", function(_, j) {
					var pos = data["bin_order"][i][j];

					return colorScale(pos);
				})

			var p = data["id_array"][i];

			d3.selectAll(".projection_point").style("opacity", 0.1);
			d3.selectAll(".projection_border").style("opacity", 0.1);
			d3.selectAll(".heatmap_row").style("opacity", 0.1);
			d3.selectAll(".projection_point.kernel_" + p).style("opacity", 1.0);
			d3.selectAll(".projection_border.kernel_" + p).style("opacity", 1.0);
			d3.selectAll(".heatmap_row.kernel_" + p).style("opacity", 1.0);
			d3.selectAll(".projection_point.kernel_" + glob_kernel).style("opacity", 1.0);
			d3.selectAll(".projection_border.kernel_" + glob_kernel).style("opacity", 1.0);
			d3.selectAll(".heatmap_row.kernel_" + glob_kernel).style("opacity", 1.0);

			// console.log(data["id_array"][i], data["intensity_max"][data["id_array"][i]]);
		})
		.on("mouseout", function() {
			if (glob_kernel == null) {
				d3.selectAll(".heatmap_row").style("opacity", 1.0);
				d3.selectAll(".projection_point").style("opacity", 1.0);
				d3.selectAll(".projection_border").style("opacity", 1.0);
			} else {
				d3.selectAll(".heatmap_row").style("opacity", 0.1);
				d3.selectAll(".projection_point").style("opacity", 0.1);
				d3.selectAll(".projection_border").style("opacity", 0.1);
				d3.selectAll(".projection_point.kernel_" + glob_kernel).style("opacity", 1.0);
				d3.selectAll(".projection_border.kernel_" + glob_kernel).style("opacity", 1.0);
				d3.selectAll(".heatmap_row.kernel_" + glob_kernel).style("opacity", 1.0);
			}
		})
		.on("click", function(_, i) {
			var p = data["id_array"].filter((v, j, a) => a.indexOf(v) === j).indexOf(data["id_array"][i])

			for (var j = 0; j < data["actv_matrix"].length; j++) {
				data["actv_matrix"][j][p] = 0;
			}

			d3.select("#kernel-selector").select("#kernel_" + data["id_array"][i]).remove("*");
			
			d3.selectAll(".projection_point.kernel_" + data["id_array"][i]).style("visibility", "hidden");
			d3.selectAll(".projection_border.kernel_" + data["id_array"][i]).style("visibility", "hidden");
			d3.selectAll(".heatmap_row.kernel_" + data["id_array"][i]).style("visibility", "hidden");

			var activations = 0;
			for (var j = 0; j < data["actv_matrix"].length; j++) {
				activations += (1 - d3.max(data["actv_matrix"][j]));
			}

			d3.select("#projection-svg").select("#activation_text").remove("*");

			d3.select("#projection-svg").append("text")
				.attr("id", "activation_text")
				.attr("x", 840)
				.attr("y", 400)
				.text("Number of Training Elements without Activation: " + activations)

			d3.selectAll(".heatmap_row").style("opacity", 1.0);
			d3.selectAll(".projection_point").style("opacity", 1.0);
			d3.selectAll(".projection_border").style("opacity", 1.0);
		});
	


	// var points = projection_g.selectAll(".projection_point")
	// 	.data(data["projection"])
	// .enter().append("g")
	// 	.attr("class", function (_, i) { 
	// 		return "projection_point kernel_" + data["id_array"][i] + " projindex_" + i;
	// 	})
	// 	.attr("transform", function (d) { 
	// 		return "translate(" + (proj_xpos + xScale(d[0])) + "," + (proj_ypos + yScale(d[1])) + ")"; 
	// 	})
	// 	.on("mouseover", function(_, i) {
	// 		d3.select("#histogram_view").selectAll(".histogram_bin").remove("*");

	// 		var n_labels = data["histograms"][i].length

	// 		var colorScale = d3.scaleOrdinal()
	// 			.domain(d3.range(n_labels))
	// 			.range(d3.schemePaired);

	// 		var max_v = d3.max(data["histograms"], function (d) { return d3.max(d, function(x) { return Math.abs(x); }); });

	// 		var binScale = d3.scaleLinear()
	// 			.domain([max_v, -max_v])
	// 			.range([hist_ypos, hist_ypos + hist_height])

	// 		var bins = histogram_g.selectAll(".histogram_bin")
	// 			.data(data["histograms"][i])
	// 		.enter().append("rect")
	// 			.attr("class", "histogram_bin")
	// 			.attr("x", function(_, j) { 
	// 				var pos = data["bin_order"][i][j];

	// 				return hist_xpos + pos*hist_width/n_labels 
	// 			})
	// 			.attr("y", function(d, _) { 
	// 				if (d > 0.0) {
	// 					return binScale(d);
	// 				} else {
	// 					return binScale(0);
	// 				}
	// 			})
	// 			.attr("width", hist_width/n_labels)
	// 			.attr("height", function(d) { 
	// 				if (d > 0.0) {
	// 					return binScale(0) - binScale(d);
	// 				} else {
	// 					return binScale(d) - binScale(0);
	// 				}
	// 			})
	// 			.style("fill", function(_, j) {
	// 				var pos = data["bin_order"][i][j];

	// 				return colorScale(pos);
	// 			})

	// 		var p = data["id_array"][i];

	// 		d3.selectAll(".projection_point").style("opacity", 0.1);
	// 		d3.selectAll(".projection_border").style("opacity", 0.1);
	// 		d3.selectAll(".heatmap_row").style("opacity", 0.1);
	// 		d3.selectAll(".projection_point.kernel_" + p).style("opacity", 1.0);
	// 		d3.selectAll(".projection_border.kernel_" + p).style("opacity", 1.0);
	// 		d3.selectAll(".heatmap_row.kernel_" + p).style("opacity", 1.0);
	// 		d3.selectAll(".projection_point.kernel_" + glob_kernel).style("opacity", 1.0);
	// 		d3.selectAll(".projection_border.kernel_" + glob_kernel).style("opacity", 1.0);
	// 		d3.selectAll(".heatmap_row.kernel_" + glob_kernel).style("opacity", 1.0);

	// 		// console.log(data["id_array"][i], data["intensity_max"][data["id_array"][i]]);
	// 	})
	// 	.on("mouseout", function() {
	// 		if (glob_kernel == null) {
	// 			d3.selectAll(".heatmap_row").style("opacity", 1.0);
	// 			d3.selectAll(".projection_point").style("opacity", 1.0);
	// 			d3.selectAll(".projection_border").style("opacity", 1.0);
	// 		} else {
	// 			d3.selectAll(".heatmap_row").style("opacity", 0.1);
	// 			d3.selectAll(".projection_point").style("opacity", 0.1);
	// 			d3.selectAll(".projection_border").style("opacity", 0.1);
	// 			d3.selectAll(".projection_point.kernel_" + glob_kernel).style("opacity", 1.0);
	// 			d3.selectAll(".projection_border.kernel_" + glob_kernel).style("opacity", 1.0);
	// 			d3.selectAll(".heatmap_row.kernel_" + glob_kernel).style("opacity", 1.0);
	// 		}
	// 	})
	// 	.on("click", function(_, i) {
	// 		var p = data["id_array"].filter((v, j, a) => a.indexOf(v) === j).indexOf(data["id_array"][i])

	// 		for (var j = 0; j < data["actv_matrix"].length; j++) {
	// 			data["actv_matrix"][j][p] = 0;
	// 		}

	// 		d3.select("#kernel-selector").select("#kernel_" + data["id_array"][i]).remove("*");
			
	// 		d3.selectAll(".projection_point.kernel_" + data["id_array"][i]).style("visibility", "hidden");
	// 		d3.selectAll(".projection_border.kernel_" + data["id_array"][i]).style("visibility", "hidden");
	// 		d3.selectAll(".heatmap_row.kernel_" + data["id_array"][i]).style("visibility", "hidden");

	// 		var activations = 0;
	// 		for (var j = 0; j < data["actv_matrix"].length; j++) {
	// 			activations += (1 - d3.max(data["actv_matrix"][j]));
	// 		}

	// 		d3.select("#projection-svg").select("text").remove("*");

	// 		d3.select("#projection-svg").append("text")
	// 			.attr("id", "activation_text")
	// 			.attr("x", 840)
	// 			.attr("y", 400)
	// 			.text("Number of Training Elements without Activation: " + activations)

	// 		d3.selectAll(".heatmap_row").style("opacity", 1.0);
	// 		d3.selectAll(".projection_point").style("opacity", 1.0);
	// 		d3.selectAll(".projection_border").style("opacity", 1.0);
	// 	});
	
	rings.append("title")
		.text(function (_, i) { return "Kernel " + data["id_array"][i]; });

	// var arc = points.selectAll(".arc")
	// 	.data(function(_, i) { return pie(data["histograms"][i]); })
	// .enter().append("g")
	// 	.attr("class", function (_, i) {
	// 		var class_string = d3.select(d3.select(this).node().parentNode).attr("class");
	// 		var pos = class_string.indexOf("projindex_") + 10;
	// 		var j = class_string.substring(pos);

	// 		return "arc arc_class_" + data["bin_order"][j][i];
	// 	});

	// arc.append("path")
	// 	.attr("d", path)
	// 	.attr("fill", function (_, i) { 
	// 		var class_string = d3.select(d3.select(this).node().parentNode.parentNode).attr("class");
	// 		var pos = class_string.indexOf("projindex_") + 10;
	// 		var j = class_string.substring(pos);
			
	// 		return colorScale(data["bin_order"][j][i]);
	// 	});

	// Drawn heatmap.
	var heat_xpos = 1360;
	var heat_ypos = 70;
	var heat_width = 460;
	var heat_height = 800;

	var heatmap_g = d3.select("#projection-svg").append("g")
		.attr("id", "heatmap_view");

	heatmap_g.append("rect")
		.attr("x", heat_xpos)
		.attr("y", heat_ypos)
		.attr("width", heat_width)
		.attr("height", heat_height)
		.style("fill", "white")
		.style("stroke", "black");

	var xScaleHeatmap = d3.scaleLinear()
		.domain([0, data["selectivity"][0].length])
		.range([0, heat_width]);

	var yScaleHeatmap = d3.scaleLinear()
		.domain([0, data["selectivity"].length])
		.range([0, heat_height])

	var colorScaleHeatmap = d3.scaleLinear()
		.domain([-1, 0, 1])
		.range(["red", "white", "blue"]);

	var rows = heatmap_g.selectAll(".heatmap_row")
		.data(data["selectivity"])
	.enter().append("g")
		.attr("class", function (_, i) { return "heatmap_row kernel_" + data["id_array"][i] + " histindex_" + i; })
		.attr("id", function (_, i) { return "row_" + i; })
		.on("mouseover", function(_, i) {
			d3.select("#histogram_view").selectAll(".histogram_bin").remove("*");

			var n_labels = data["selectivity"][i].length

			var colorScale = d3.scaleOrdinal()
				.domain(d3.range(n_labels))
				.range(d3.schemePaired);

			var max_v = d3.max(data["selectivity"], function (d) { return d3.max(d, function(x) { return Math.abs(x); }); });

			var binScale = d3.scaleLinear()
				.domain([max_v, -max_v])
				.range([hist_ypos, hist_ypos + hist_height])

			var bins = histogram_g.selectAll(".histogram_bin")
				.data(data["selectivity"][i])
			.enter().append("rect")
				.attr("class", "histogram_bin")
				.attr("x", function(_, j) { 
					var pos = data["bin_order"][i][j];

					return hist_xpos + pos*hist_width/n_labels 
				})
				.attr("y", function(d, _) { 
					if (d > 0.0) {
						return binScale(d);
					} else {
						return binScale(0);
					}
				})
				.attr("width", hist_width/n_labels)
				.attr("height", function(d) { 
					if (d > 0.0) {
						return binScale(0) - binScale(d);
					} else {
						return binScale(d) - binScale(0);
					}
				})
				.style("fill", function(_, j) {
					var pos = data["bin_order"][i][j];

					return colorScale(pos);
				})

			d3.selectAll(".projection_point").style("opacity", 0.1);
			d3.selectAll(".projection_border").style("opacity", 0.1);
			d3.selectAll(".projection_point.kernel_" + data["id_array"][i]).style("opacity", 1.0);
			d3.selectAll(".projection_border.kernel_" + data["id_array"][i]).style("opacity", 1.0);
		})
		.on("mouseout", function() {
			d3.selectAll(".projection_point").style("opacity", 1.0);
			d3.selectAll(".projection_border").style("opacity", 1.0);
		});

	rows.append("title")
		.text(function (_, i) { return "Kernel " + data["id_array"][i]; });

	rows.selectAll(".heatmap_cell")
		.data(function (d) { return d; })
	.enter().append("rect")
		.attr("class", "heatmap_cell")
		.attr("x", function (_, i) { 
			var class_string = d3.select(d3.select(this).node().parentNode).attr("class");
			var pos = class_string.indexOf("histindex_") + 10;
			var j = class_string.substring(pos);

			return heat_xpos + xScaleHeatmap(data["bin_order"][j][i]); 
		})
		.attr("y", function () { 
			var row = parseInt(d3.select(this.parentNode).attr("id").substring(4));
			return heat_ypos + yScaleHeatmap(row);
		})
		.attr("width", heat_width / data["histograms"][0].length)
		.attr("height", heat_height / data["histograms"].length)
		.style("fill", function (d) { return colorScaleHeatmap(d); });

	var class_boxes = heatmap_g.selectAll(".class_box")
		.data(data["histograms"][0])
	.enter().append("rect")
		.attr("class", "class_box")
		.attr("id", function (_, i) { return "classbox_" + i; })
		.attr("x", function (_, i) { return heat_xpos + xScaleHeatmap(i + 0.15); })
		.attr("y", heat_ypos + heat_height + 10)
		.attr("width", heat_width / data["histograms"][0].length * 0.75)
		.attr("height", 30)
		.style("fill", function (_, i) { return d3.schemePaired[i]})
		.style("stroke", "black")
		.on("click", function (_, i) {
			if (selected_classes.length == 0 || $.inArray(i, selected_classes) == -1) {
				selected_classes.push(i);

				if (highlight == "any_class") {
					d3.selectAll(".heatmap_row").style("opacity", 0.0);
					d3.selectAll(".class_box").style("opacity", 0.25);

					for (var j = 0; j < selected_classes.length; j++) {
						var x = selected_classes[j];

						d3.selectAll(".heatmap_row").filter(function (_, i) {
							var pos = data["bin_order"][i].indexOf(x);

							return d3.select(this).data()[0][pos] > 0; 
						}).style("opacity", 1.0);
						
						d3.selectAll("#classbox_" + x).style("opacity", 1.0);
					}
				} else if (highlight == "all_class") {
					d3.selectAll(".heatmap_row").style("opacity", 1.0);
					d3.selectAll(".class_box").style("opacity", 0.25);

					for (var j = 0; j < selected_classes.length; j++) {
						var x = selected_classes[j];

						d3.selectAll(".heatmap_row").filter(function (_, i) { 
							var pos = data["bin_order"][i].indexOf(x);
							
							return d3.select(this).data()[0][pos] <= 0;
						}).style("opacity", 0.0);
						
						d3.selectAll("#classbox_" + x).style("opacity", 1.0);
					}
				} else {
					if (selected_classes.length == 2) {
						d3.selectAll(".heatmap_row").style("opacity", 0.0);
						d3.selectAll(".class_box").style("opacity", 0.1);

						var a = selected_classes[0];
						var b = selected_classes[1];

						d3.selectAll(".heatmap_row").filter(function (_, i) {
							var pos_a =  data["bin_order"][i].indexOf(a);
							var pos_b =  data["bin_order"][i].indexOf(b);
							
							return d3.select(this).data()[0][pos_a] > 0 && d3.select(this).data()[0][pos_b] <= 0; 
						}).style("opacity", 1.0);

						d3.selectAll("#classbox_" + a).style("opacity", 1.0);
						d3.selectAll("#classbox_" + b).style("opacity", 0.5);
					} else {
						alert("Please, choose exactly two classes.")
					}
				}
			} else {
				selected_classes = selected_classes.filter(function (x) { return x != i; });

				if (selected_classes.length == 0) {
					d3.selectAll(".heatmap_row").style("opacity", 1.0);
					d3.selectAll(".class_box").style("opacity", 1.0);
				} else {
					if (highlight == "any_class") {
						d3.selectAll(".heatmap_row").style("opacity", 0.0);
						d3.selectAll(".class_box").style("opacity", 0.25);

						for (var j = 0; j < selected_classes.length; j++) {
							var x = selected_classes[j];

							d3.selectAll(".heatmap_row").filter(function (_, i) { 
								var pos = data["bin_order"][i].indexOf(x);

								return d3.select(this).data()[0][pos] > 0; 
							}).style("opacity", 1.0);
							
							d3.selectAll("#classbox_" + x).style("opacity", 1.0);
						}
					} else if (highlight == "all_class") {
						d3.selectAll(".heatmap_row").style("opacity", 1.0);
						d3.selectAll(".class_box").style("opacity", 0.25);

						for (var j = 0; j < selected_classes.length; j++) {
							var x = selected_classes[j];

							d3.selectAll(".heatmap_row").filter(function (_, i) {
								var pos = data["bin_order"][i].indexOf(x);

								return d3.select(this).data()[0][pos] <= 0; 
							}).style("opacity", 0.0);
							
							d3.selectAll("#classbox_" + x).style("opacity", 1.0);
						}
					} else {
						if (selected_classes.length == 2) {
							d3.selectAll(".heatmap_row").style("opacity", 0.0);
							d3.selectAll(".class_box").style("opacity", 0.1);

							var a = selected_classes[0];
							var b = selected_classes[1];

							d3.selectAll(".heatmap_row").filter(function (_, i) {
								var pos_a =  data["bin_order"][i].indexOf(a);
								var pos_b =  data["bin_order"][i].indexOf(b);
							 
								return d3.select(this).data()[0][pos_a] > 0 && d3.select(this).data()[0][pos_b] <= 0; 
							}).style("opacity", 1.0);

							d3.selectAll("#classbox_" + a).style("opacity", 1.0);
							d3.selectAll("#classbox_" + b).style("opacity", 0.5);
						} else {
							alert("Please, choose exactly two classes.")
						}
					}
				}
			}
		});

	var class_texts = heatmap_g.selectAll(".class_text")
		.data(data["histograms"][0])
	.enter().append("text")
		.attr("class", "class_texts")
		.attr("x", function (_, i) { return heat_xpos + xScaleHeatmap(i + 0.4); })
		.attr("y", heat_ypos + heat_height + 30)
		.text(function (_, i) { return i; });

	// Boxes to display the classes that are activating for a least one position of the selected kernel.
	var kernel_class_boxes = projection_g.selectAll(".kernel_class_box")
		.data(data["histograms"][0])
	.enter().append("rect")
		.attr("class", "kernel_class_box")
		.attr("id", function (_, i) { return "kernel_classbox_" + i; })
		.attr("x", function (_, i) { return proj_xpos + i*50; })
		.attr("y", proj_ypos - 50)
		.attr("width", 30)
		.attr("height", 30)
		.style("fill", function (_, i) { return d3.schemePaired[i]})
		.style("stroke", "black")
		.on("click", function (_, i) {
			if (selected_slice == i) {
				d3.selectAll(".projection_border").style("opacity", 1.0);
				d3.selectAll(".arc").style("opacity", 1.0);
				selected_slice = null;
			} else {
				d3.selectAll(".projection_border").style("opacity", 0.0);
				d3.selectAll(".arc").style("opacity", 0.1);
				d3.selectAll(".arc_class_" + i).style("opacity", 1.0);
				selected_slice = i;
			}
		});

	var class_texts = projection_g.selectAll(".kernel_class_text")
		.data(data["histograms"][0])
	.enter().append("text")
		.attr("class", "kernel_class_texts")
		.attr("x", function (_, i) { return proj_xpos + i*50 + 10; })
		.attr("y", proj_ypos - 30)
		.text(function (_, i) { return i; });
});

$(document).ready(function() {
	$("#kernel-selector").change(function() {
		glob_kernel = parseInt(this.value);

		if (glob_kernel < 0) {
			glob_kernel = null;
			d3.selectAll(".projection_point").style("opacity", 1.0);
			d3.selectAll(".projection_border").style("opacity", 1.0);
			d3.selectAll(".heatmap_row").style("opacity", 1.0);
			d3.selectAll(".kernel_class_box").style("opacity", 1.0);
		} else {
			d3.selectAll(".projection_point").style("opacity", 0.1);
			d3.selectAll(".projection_border").style("opacity", 0.1);
			d3.selectAll(".heatmap_row").style("opacity", 0.1);
			d3.selectAll(".projection_point.kernel_" + glob_kernel).style("opacity", 1.0);
			d3.selectAll(".projection_border.kernel_" + glob_kernel).style("opacity", 1.0);
			d3.selectAll(".heatmap_row.kernel_" + glob_kernel).style("opacity", 1.0);

			d3.selectAll(".kernel_class_box").style("opacity", 0.1);
			d3.selectAll(".kernel_class_box").filter(function (_, i) {
				var b = false;

				for (var j = 0; j < data["histograms"].length; j++) {
					if (data["id_array"][j] == glob_kernel) {
						var pos = data["bin_order"][j].indexOf(i);
						b = b || data["histograms"][j][pos] > 0.0;
					}
				}

				return b;
			}).style("opacity", 1.0);
		}
	});
});

$(document).ready(function() {
	$("#highlight-selector").change(function() {
		highlight = this.value;

		if (selected_classes.length == 0) {
			d3.selectAll(".heatmap_row").style("opacity", 1.0);
			d3.selectAll(".class_box").style("opacity", 1.0);
		} else {
			if (highlight == "any_class") {
				d3.selectAll(".heatmap_row").style("opacity", 0.0);
				d3.selectAll(".class_box").style("opacity", 0.25);

				for (var j = 0; j < selected_classes.length; j++) {
					var x = selected_classes[j];

					d3.selectAll(".heatmap_row").filter(function (_, i) { 
						var pos = data["bin_order"][i].indexOf(x);

						return d3.select(this).data()[0][pos] > 0;
					}).style("opacity", 1.0);
					
					d3.selectAll("#classbox_" + x).style("opacity", 1.0);
				}
			} else if (highlight == "all_class") {
				d3.selectAll(".heatmap_row").style("opacity", 1.0);
				d3.selectAll(".class_box").style("opacity", 0.25);

				for (var j = 0; j < selected_classes.length; j++) {
					var x = selected_classes[j];

					d3.selectAll(".heatmap_row").filter(function (_, i) {
						var pos = data["bin_order"][i].indexOf(x);

						return d3.select(this).data()[0][pos] <= 0; 
					}).style("opacity", 0.0);
					
					d3.selectAll("#classbox_" + x).style("opacity", 1.0);
				}
			} else {
				if (selected_classes.length == 2) {
					d3.selectAll(".heatmap_row").style("opacity", 0.0);
					d3.selectAll(".class_box").style("opacity", 0.1);

					var a = selected_classes[0];
					var b = selected_classes[1];

					d3.selectAll(".heatmap_row").filter(function (_, i) { 
						var pos_a =  data["bin_order"][i].indexOf(a);
						var pos_b =  data["bin_order"][i].indexOf(b);

						return d3.select(this).data()[0][pos_a] > 0 && d3.select(this).data()[0][pos_b] <= 0; 
					}).style("opacity", 1.0);

					d3.selectAll("#classbox_" + a).style("opacity", 1.0);
					d3.selectAll("#classbox_" + b).style("opacity", 0.5);
				} else {
					alert("Please, choose exactly two classes.")
				}
			}
		}
	});
});