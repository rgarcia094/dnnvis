// Update the layer selection buttons with the number of layers in the model.
function update_layer_selectors(model_info) {
	d3.select("#layer-selector").selectAll("option")
		.data(model_info["layers"])
	.enter().append("option")
		.attr("value", function(_, i) { return i; })
		.text(function(d, i) { return "Layer " + i + ": " + d["layer_type"]; });

	// Last layer is the default.
	$("#layer-selector").val(model_info["layers"].length - 1)
}

function update_sorting_selectors(model_info) {
	for (var i = 0; i < model_info["n_labels"]; i++) {
		d3.select("#sorting-selector").append("option")
			.attr("value", "label" + i)
			.text("Sort matrix using by label " + i);
	}
}

/* Label Histograms */

// Build a heatmap matrix with the label histograms of a layer.
function build_histogram_heatmap(layer_info) {
	var x_pos = 75;
	var y_pos = 50;
	var width = 300;
	var height = 840;
	var u_height = height/layer_info["unit_order"].length;
	var n_labels = layer_info["labels"];
	var b_width = width/n_labels;

	var matrix_histogram_g = d3.select("#dnnvis-svg").append("g")
		.attr("id", "label_histogram");

	matrix_histogram_g.append("rect")
		.attr("x", x_pos - 1)
		.attr("y", y_pos - 1)
		.attr("width", width + 2)
		.attr("height", height + 2)
		.style("fill", "white")
		.style("stroke", "black");

	var transition = d3.transition()
		.duration(500)
		.ease(d3.easeLinear);

	var boxColorScale = d3.scaleOrdinal()
		.domain(d3.range(n_labels))
		.range(d3.schemeCategory10);

	matrix_histogram_g.selectAll(".label-text-box")
		.data(d3.range(0, n_labels))
	.enter().append("rect")
		.attr("class", "label-text-box")
		.attr("value", function (d) { return d; })
		.attr("x", function (d) { return x_pos + b_width*(d+0.2); })
		.attr("y", y_pos - 25)
		.attr("width", 20)
		.attr("height", 20)
		.style("fill", function (d) { return boxColorScale(d); })
		.style("stroke", "black");

	matrix_histogram_g.selectAll(".label_name")
		.data(d3.range(0, n_labels))
	.enter().append("text")
		.attr("class", "label_name")
		.attr("x", function (_, j) { return x_pos + b_width*(j+0.4); })
		.attr("y", y_pos - 10)
		.text(function (d) { return d; });

	var max_value = d3.max(layer_info["histograms"], function(row) { return d3.max(row); });
	var min_value = d3.min(layer_info["histograms"], function(row) { return d3.min(row); });

	var colorScale = d3.scaleLinear()
		.domain([Math.min(0, min_value), max_value])
		.range(["white", "navy"])

	for (var i = 0; i < layer_info["histograms"].length; i++) {
		matrix_histogram_g.append("text")
			.attr("id", "unit_text" + layer_info["unit_order"][i])
			.attr("x", x_pos - 55)
			.attr("y", y_pos + u_height*(i+0.6))
			.text("Unit " + layer_info["unit_order"][i])

		for (var j = 0; j < layer_info["histograms"][i].length; j++) {
			var color = colorScale(layer_info["histograms"][i][j]);

			matrix_histogram_g.append("rect")
				.attr("class", "heatmap-cell unit" + layer_info["unit_order"][i] + " label" + j)
				.attr("value", layer_info["histograms"][i][j])
				.attr("x", x_pos + b_width*j)
				.attr("y", y_pos + u_height*i)
				.attr("width", b_width)
				.attr("height", u_height)
				.attr("fill", color)
				.attr("stroke", color)
				.on("mouseover", function() {
					var unit = parseInt(d3.select(this).attr("class").split(" ")[1].substring(4));
					
					d3.selectAll(".projection_point").style("fill-opacity", 0.25);
					d3.selectAll(".projection_point").style("stroke-opacity", 0.25);
					
					d3.select("#proj-unit" + unit).style("fill-opacity", 1.0);
					d3.select("#proj-unit" + unit).style("stroke-opacity", 1.0);
				})
				.on("mouseout", function() {
					d3.selectAll(".projection_point").style("fill-opacity", 1.0);
					d3.selectAll(".projection_point").style("stroke-opacity", 1.0);
				})
				.on("click", function() {
					var unit = parseInt(d3.select(this).attr("class").split(" ")[1].substring(4));
					request_activation_dispersion(layer, unit)
				});
		}
	}
}

function remove_histogram_heatmap() {
	d3.select("#label_histogram").remove("*");
}

function argMax(array) {
	return array.map((x, i) => [x, i]).reduce((r, a) => (a[0] > r[0] ? a : r))[1];
}

function build_intralayer_projection(layer_info) {
	var x_pos = 400;
	var y_pos = 50;
	var size = 840;
	var n_labels = layer_info["labels"];

	var projection_g = d3.select("#dnnvis-svg").append("g")
		.attr("id", "projection_view");

	projection_g.append("rect")
		.attr("x", x_pos)
		.attr("y", y_pos)
		.attr("width", size)
		.attr("height", size)
		.style("fill", "white")
		.style("stroke", "black");

	var min_x = d3.min(layer_info["projection"], function(d) { return d[0]; });
	var max_x = d3.max(layer_info["projection"], function(d) { return d[0]; });
	var min_y = d3.min(layer_info["projection"], function(d) { return d[1]; });
	var max_y = d3.max(layer_info["projection"], function(d) { return d[1]; });

	var xScale = d3.scaleLinear()
		.domain([min_x, max_x])
		.range([25, size-25]);

	var yScale = d3.scaleLinear()
		.domain([min_y, max_y])
		.range([25, size-25]);

	var colorScale = d3.scaleOrdinal()
		.domain(d3.range(n_labels))
		.range(d3.schemeCategory10);

	var pie = d3.pie()
		.sort(null)
		.value(function(d) { return d; });

	var path = d3.arc()
		.outerRadius(10)
		.innerRadius(0);

	// var points = projection_g.selectAll(".projection_point")
	// 	.data(layer_info["projection"])
	// .enter().append("circle")
	// 	.attr("class", "projection_point")
	// 	.attr("id", function (_, i) { return "proj-unit" + layer_info["unit_order"][i]; })
	// 	.attr("cx", function (d) { return x_pos + xScale(d[0]); })
	// 	.attr("cy", function (d) { return y_pos + yScale(d[1]); })
	// 	.attr("r", 7.5)
	// 	.style("fill", function (_, i) {
	// 		var r = 0.0, g = 0.0, b = 0.0;
	// 		var tr = 0.0, tg = 0.0, tb = 0.0;

	// 		for (var j = 0; j < n_labels; j++) {
	// 			tr += layer_info["histograms"][i][j];
	// 			tg += layer_info["histograms"][i][j];
	// 			tb += layer_info["histograms"][i][j];
	// 		}

	// 		for (var j = 0; j < n_labels; j++) {
	// 			var c = d3.rgb(colorScale(j));

	// 			console.log(c.r, layer_info["histograms"][i][j]);

	// 			r += c.r*layer_info["histograms"][i][j]/tr;
	// 			g += c.g*layer_info["histograms"][i][j]/tg;
	// 			b += c.b*layer_info["histograms"][i][j]/tb;
	// 		}

	// 		console.log(r, g, b)
	// 		return d3.rgb(r, g, b).toString();
	// 	})
	// 	.style("stroke", "black")
	// 	.on("mouseover", function (_, i) {
	// 		var row_y = d3.selectAll(".unit" + layer_info["unit_order"][i]).attr("y");
	// 		var row_x = 75;
	// 		var width = 300;
	// 		var height = 840/layer_info["projection"].length;

	// 		d3.select("#label_histogram").append("rect")
	// 			.attr("id", "selected-row")
	// 			.attr("x", row_x)
	// 			.attr("y", row_y)
	// 			.attr("width", width)
	// 			.attr("height", height)
	// 			.style("fill-opacity", 0)
	// 			.style("stroke", "red");

	// 		build_histogram_chart(layer_info["histograms"][i])
	// 	})
	// 	.on("mouseout", function() { 
	// 		d3.select("#selected-row").remove("*"); 
	// 	})
	// 	.on("click", function(_, i) {
	// 		var unit = layer_info["unit_order"][i];
	// 		request_activation_dispersion(layer, unit)
	// 	});

	var points = projection_g.selectAll(".projection_point")
		.data(layer_info["projection"])
	.enter().append("g")
		.attr("class", "projection_point")
		.attr("id", function (_, i) { return "proj-unit" + layer_info["unit_order"][i]; })
		.attr("transform", function (d) { return "translate(" + (x_pos + xScale(d[0])) + "," + (y_pos + yScale(d[1])) + ")"; })
		.on("mouseover", function (_, i) {
			var row_y = d3.selectAll(".unit" + layer_info["unit_order"][i]).attr("y");
			var row_x = 75;
			var width = 300;
			var height = 840/layer_info["projection"].length;

			d3.select("#label_histogram").append("rect")
				.attr("id", "selected-row")
				.attr("x", row_x)
				.attr("y", row_y)
				.attr("width", width)
				.attr("height", height)
				.style("fill-opacity", 0)
				.style("stroke", "red");

			build_histogram_chart(layer_info["histograms"][i])

			var unit = layer_info["unit_order"][i];
			
			d3.selectAll(".projection_point").attr("fill-opacity", 0.1);
			d3.select(this).attr("fill-opacity", 1.0);
		})
		.on("mouseout", function() { 
			d3.select("#selected-row").remove("*");

			d3.selectAll(".projection_point").attr("fill-opacity", 1.0);
		})
		.on("click", function(_, i) {
			var unit = layer_info["unit_order"][i];
			request_activation_dispersion(layer, unit)
		});

	var arc = points.selectAll(".arc")
		.data(function(_, i) { return pie(layer_info["histograms"][i]); })
	.enter().append("g")
		.attr("class", "arc");

	arc.append("path")
		.attr("d", path)
		.attr("fill", function (_, i) { return colorScale(i); });
}

function remove_projection() {
	d3.selectAll("#projection_view").remove("*");
}

// Display a plot with the dispersion of activations for each class.
function build_activation_dispersion(unit_info) {
	var x_pos = 1275;
	var y_pos = 50;
	var size = 500;
	var n_labels = d3.max(unit_info["actual_labels"]) + 1;

	var dispersion_g = d3.select("#dnnvis-svg").append("g")
		.attr("id", "dispersion_view");

	dispersion_g.append("rect")
		.attr("x", x_pos)
		.attr("y", y_pos)
		.attr("width", size)
		.attr("height", size)
		.style("fill", "white")
		.style("stroke", "black");

	var min_actv = d3.min(unit_info["activations"]);
	var max_actv = d3.max(unit_info["activations"]);

	var actvScale = d3.scaleLinear()
		.domain([min_actv, max_actv])
		.range([size, 0]);

	var labelScale = d3.scaleLinear()
		.domain([0, n_labels])
		.range([25, size+25]);

	var colorScale = d3.scaleOrdinal()
		.domain(d3.range(n_labels))
		.range(d3.schemeCategory10);

	dispersion_g.selectAll(".activation_points")
		.data(unit_info["activations"])
	.enter().append("circle")
		.attr("class", "activation_points")
		.attr("r", function (_, i) { return unit_info["actual_labels"][i] == unit_info["predicted_labels"][i]? 5 : 10; })
		.attr("cx", function (_, i) { return x_pos + labelScale(unit_info["actual_labels"][i]); })
		.attr("cy", function (d) { return y_pos + actvScale(d); })
		.style("fill", function (_, i) { return colorScale(unit_info["predicted_labels"][i]); })
		.style("stroke", "black");

	var actvAxis = d3.axisLeft(actvScale)
		.ticks(10);

	dispersion_g.append("g")
		.attr("class", "actv_axis")
		.attr("transform", "translate(" + x_pos + "," + y_pos + ")")
		.call(actvAxis);

	dispersion_g.selectAll(".label-text-box")
		.data(d3.range(0, n_labels))
	.enter().append("rect")
		.attr("class", "label-text-box")
		.attr("value", function (d) { return d; })
		.attr("x", function (d) { return x_pos + labelScale(d) - 10; })
		.attr("y", y_pos + size + 25)
		.attr("width", 20)
		.attr("height", 20)
		.style("fill", function (d) { return colorScale(d); })
		.style("stroke", "black");

	dispersion_g.selectAll(".label_name")
		.data(d3.range(0, n_labels))
	.enter().append("text")
		.attr("class", "label_name")
		.attr("x", function (d) { return x_pos + labelScale(d) - 5; })
		.attr("y", y_pos + size + 40)
		.text(function (d) { return d; });
}

function remove_dispersion() {
	d3.selectAll("#dispersion_view").remove("*");
}

function build_histogram_chart(histogram) {
	remove_histogram();

	var x_pos = 1275;
	var y_pos = 600;
	var width = 500;
	var height = 285;
	var n_labels = histogram.length;

	var histogram_g = d3.select("#dnnvis-svg").append("g")
		.attr("id", "histogram_chart");

	histogram_g.append("rect")
		.attr("x", x_pos)
		.attr("y", y_pos)
		.attr("width", width)
		.attr("height", height)
		.style("fill", "white")
		.style("stroke", "black");

	var min_actv = d3.min(histogram);
	var max_actv = d3.max(histogram);

	var actvScale = d3.scaleLinear()
		.domain([min_actv, max_actv])
		.range([height, 0]);

	var labelScale = d3.scaleLinear()
		.domain([0, n_labels])
		.range([0, width]);

	var colorScale = d3.scaleOrdinal()
		.domain(d3.range(n_labels))
		.range(d3.schemeCategory10);

	histogram_g.selectAll(".barchart")
		.data(histogram)
	.enter().append("rect")
		.attr("class", "barchart")
		.attr("x", function (_, i) { return x_pos + labelScale(i); })
		.attr("y", function (d) { return y_pos + actvScale(d); })
		.attr("width", width/n_labels)
		.attr("height", function (d) { return height - actvScale(d); })
		.style("fill", function (_, i) { return colorScale(i); });

	var actvAxis = d3.axisLeft(actvScale)
		.ticks(10);

	histogram_g.append("g")
		.attr("class", "actv_axis")
		.attr("transform", "translate(" + x_pos + "," + y_pos + ")")
		.call(actvAxis);
}

function remove_histogram() {
	d3.selectAll("#histogram_chart").remove("*");
}

function buid_interlayer_projection(model_info) {
	var x_pos = 200;
	var y_pos = 50;
	var size = 840;
	var n_labels = model_info["labels"];

	var projection_g = d3.select("#dnnvis-svg").append("g")
		.attr("id", "projection_view");

	projection_g.append("rect")
		.attr("x", x_pos)
		.attr("y", y_pos)
		.attr("width", size)
		.attr("height", size)
		.style("fill", "white")
		.style("stroke", "black");

	var min_x = d3.min(model_info["projection"], function(d) { return d[0]; });
	var max_x = d3.max(model_info["projection"], function(d) { return d[0]; });
	var min_y = d3.min(model_info["projection"], function(d) { return d[1]; });
	var max_y = d3.max(model_info["projection"], function(d) { return d[1]; });

	var xScale = d3.scaleLinear()
		.domain([min_x, max_x])
		.range([25, size-25]);

	var yScale = d3.scaleLinear()
		.domain([min_y, max_y])
		.range([25, size-25]);

	var colorScale = d3.scaleOrdinal()
		.domain(d3.range(n_labels))
		.range(d3.schemeCategory10);

	projection_g.selectAll(".projection_point")
		.data(model_info["projection"])
	.enter().append("circle")
		.attr("class", function (_, i) {
			var k = 0;
			for (var j = 1; j < model_info["layer_start"].length; j++) {
				if (model_info["layer_start"][j] > i) {
					k = j-1;
					break;
				} else if (j == model_info["layer_start"].length-1) {
					k = j;
				}
			}

			return "projection_point layer" + k;
		})
		.attr("cx", function (d) { return x_pos + xScale(d[0]); })
		.attr("cy", function (d) { return y_pos + yScale(d[1]); })
		.attr("r", 10)
		.style("fill", function(_, i) {
			max_index = argMax(model_info["histograms"][i]);
			return colorScale(max_index);
		})
		.style("stroke", "black")
		.on("click", function(_, i) {
			var u = model_info["unit_order"][i];
			var l = 0;
			for (var j = 1; j < model_info["layer_start"].length; j++) {
				if (model_info["layer_start"][j] > i) {
					l = j-1;
					break;
				} else if (j == model_info["layer_start"].length-1) {
					l = j;
				}
			}

			if (scattLayer == -1) {
				scattLayer = l;
				scattUnit = u;
			} else {
				request_activation_scatterplot(scattLayer, scattUnit, l, u);
				scattLayer = -1;
				scattUnit = -1;
			}
		});

	projection_g.selectAll(".layer_selector")
		.data(model_info["layer_start"])
	.enter().append("rect")
		.attr("class", "layer_selector")
		.attr("id", function (_, i) { return "layer_selector" + i; })
		.attr("x", x_pos - 75)
		.attr("y", function (_, i) { return y_pos + 50*i; })
		.attr("width", 60)
		.attr("height", 25)
		.style("fill", "white")
		.style("stroke", "black")
		.on("click", function (_, i) {
			d3.selectAll(".projection_point").style("opacity", 0.1);
			d3.selectAll(".projection_point.layer" + i).style("opacity", 1.0);
		});

	projection_g.selectAll(".layer_text")
		.data(model_info["layer_start"])
	.enter().append("text")
		.attr("class", "layer_text")
		.attr("x", x_pos - 75)
		.attr("y", function (_, i) { return y_pos + 50*i + 20; })
		.text(function (_, i) { return "Layer " + i; });
}

function build_activation_scatterplot(model_info) {
	var x_pos = 1250;
	var y_pos = 50;
	var size = 500;
	var n_labels = d3.max(model_info["unit1"]["actual_labels"]) + 1;

	var scatterplot_g = d3.select("#dnnvis-svg").append("g")
		.attr("id", "activation_scatterplot");

	scatterplot_g.append("rect")
		.attr("x", x_pos)
		.attr("y", y_pos)
		.attr("width", size)
		.attr("height", size)
		.style("fill", "white")
		.style("stroke", "black");

	var min_actv_u1 = d3.min(model_info["unit1"]["activations"]);
	var max_actv_u1 = d3.max(model_info["unit1"]["activations"]);

	var min_actv_u2 = d3.min(model_info["unit2"]["activations"]);
	var max_actv_u2 = d3.max(model_info["unit2"]["activations"]);

	var xScale = d3.scaleLinear()
		.domain([min_actv_u1, max_actv_u1])
		.range([0, size]);

	var yScale = d3.scaleLinear()
		.domain([min_actv_u2, max_actv_u2])
		.range([size, 0]);

	var colorScale = d3.scaleOrdinal()
		.domain(d3.range(n_labels))
		.range(d3.schemeCategory10);

	scatterplot_g.selectAll(".activation_points")
		.data(d3.range(model_info["unit1"]["activations"].length))
	.enter().append("circle")
		.attr("class", "activation_points")
		.attr("r", function (_, i) { return model_info["unit1"]["actual_labels"][i] == model_info["unit1"]["predicted_labels"][i]? 5 : 10; })
		.attr("cx", function (_, i) { return x_pos + xScale(model_info["unit1"]["activations"][i]); })
		.attr("cy", function (_, i) { return y_pos + yScale(model_info["unit2"]["activations"][i]); })
		.style("fill", function (_, i) { return colorScale(model_info["unit2"]["actual_labels"][i]); })
		.style("stroke", "black");

	var xAxis = d3.axisBottom(xScale)
		.ticks(10);

	scatterplot_g.append("g")
		.attr("class", "x_axis")
		.attr("transform", "translate(" + x_pos + "," + (y_pos + size) + ")")
		.call(xAxis);

	var yAxis = d3.axisLeft(yScale)
		.ticks(10);

	scatterplot_g.append("g")
		.attr("class", "y_axis")
		.attr("transform", "translate(" + x_pos + "," + y_pos + ")")
		.call(yAxis);

	scatterplot_g.append("text")
		.attr("x", x_pos - 100)
		.attr("y", y_pos + size/2)
		.attr("text-anchor", "middle")
		.text("Layer " + model_info["unit2"]["layer"] + " | Unit " + model_info["unit2"]["unit"]);

	scatterplot_g.append("text")
		.attr("x", x_pos + size/2)
		.attr("y", y_pos + size + 50)
		.attr("text-anchor", "middle")
		.text("Layer " + model_info["unit1"]["layer"] + " | Unit " + model_info["unit1"]["unit"]);
}

function remove_activation_scatterplot() {
	d3.selectAll("#activation_scatterplot").remove("*");
}

function build_hiddenstate_projection(layer_info) {
	var x_pos = 200;
	var y_pos = 50;
	var size = 840;
	var n_labels = layer_info["number_labels"];

	var projection_g = d3.select("#dnnvis-svg").append("g")
		.attr("id", "projection_view");

	projection_g.append("rect")
		.attr("x", x_pos)
		.attr("y", y_pos)
		.attr("width", size)
		.attr("height", size)
		.style("fill", "white")
		.style("stroke", "black");

	var min_value = d3.min(layer_info["projection"], function (u) { return d3.min(u); });
	var max_value = d3.max(layer_info["projection"], function (u) { return d3.max(u); });

	var posScale = d3.scaleLinear()
		.domain([min_value, max_value])
		.range([25, size-25]);

	var colorScale = d3.scaleOrdinal()
		.domain(d3.range(n_labels))
		.range(d3.schemeCategory10);

	projection_g.selectAll(".projection_point")
		.data(layer_info["projection"])
	.enter().append("circle")
		.attr("class", "projection_point")
		.attr("cx", function (d) { return x_pos + posScale(d[0]); })
		.attr("cy", function (d) { return y_pos + posScale(d[1]); })
		.attr("r", 10)
		.style("fill", "red")
		.style("stroke", "black");
}