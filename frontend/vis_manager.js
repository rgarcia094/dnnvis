// Padding between model and other visualization elements
var model_padding_top = 50;
var model_padding_left = 50;

// Padding between layers.
var layer_padding = 50;

// Dimension of the layer drawing in the visualization.
var layer_width = 500;
var layer_height = 100;

// Projection start position and dimensions
var projection_x = 900;
var projection_y = 100;
var projection_size = 700;

// Get layer x position in the visualization.
function get_layer_x_position(layer) {
	return model_padding_left;
}

// Get layer y position in the visualization.
function get_layer_y_position(layer) {
	return model_padding_top + layer*(layer_height + layer_padding);
}

Array.prototype.subarray=function(start,end){
     if(!end){ end=-1;} 
    return this.slice(start, this.length+1-(end*-1));
}