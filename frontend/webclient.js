// Handles the communication with back-end server.
var ws = new WebSocket("ws://localhost:8080/ws");
var sorting = "unit";
var projection = "t-sne";
var layer = 0;
var vis = "intralayer-projection";

var scattLayer = -1;
var scattUnit = -1;

// Open Websocket callback.
ws.onopen = function(evt) { 
	console.log("***Connection Opened***");
};

// Decides which action should be taken after receiving message from back-end.
ws.onmessage = function(evt) {
	console.log("Message Received... ");

	// Decodes the JSON string received from back-end.
	var dict_message = JSON.parse(evt.data);

	console.log(dict_message)
	
	// Choose the action based on the message received.
	if (dict_message["id"] == "model_info") {
		// Update the selection buttons with the layers the model has.
		update_layer_selectors(dict_message);
		update_sorting_selectors(dict_message);

		// Request label histogram data of the model's last layer.
		layer = dict_message["layers"].length-1
		request_intralayer_projection();
	} else if (dict_message["id"] == "IntraLayerProjection") {
		remove_dispersion();
		remove_activation_scatterplot();
		remove_histogram_heatmap();
		remove_projection();
		remove_histogram();

		build_histogram_heatmap(dict_message);
		build_intralayer_projection(dict_message)
	} else if (dict_message["id"] == "InterLayerProjection") {
		remove_dispersion();
		remove_activation_scatterplot();
		remove_histogram_heatmap();
		remove_projection();
		remove_histogram();

		buid_interlayer_projection(dict_message);
	} else if (dict_message["id"] == "ActivationDispersion" && vis == "intralayer-projection") {
		remove_dispersion();

		build_activation_dispersion(dict_message);
	} else if (dict_message["id"] == "ActivationScatterplot") {
		remove_activation_scatterplot();

		build_activation_scatterplot(dict_message);
	} else if (dict_message["id"] == "HiddenStateProjection") {
		remove_dispersion();
		remove_activation_scatterplot();
		remove_histogram_heatmap();
		remove_projection();
		remove_histogram();

		build_hiddenstate_projection(dict_message);		
	}
};

// Create a message requesting intra-layer label histogram data and send it to backend.
function request_intralayer_projection() {
	var msg = {
		"id": "intra_layer_projection",
		"layer": layer,
		"sorting": sorting,
		"projection": projection
	};

	sent_message(msg);
}

function request_interlayer_projection() {
	var msg = {
		"id": "inter_layer_projection",
		"projection": projection
	};

	sent_message(msg);
}

function request_hiddenstate_projection() {
	var msg = {
		"id": "hidden_state_projection",
		"layer": layer,
		"projection": projection
	}

	sent_message(msg)
}

function request_activation_dispersion(layer, unit) {
	var msg = {
		"id": "activation_dispersion",
		"unit": unit,
		"layer": layer
	};

	sent_message(msg);
}

function request_activation_scatterplot(layer1, unit1, layer2, unit2) {
	var msg = {
		"id": "activation_scatterplot",
		"unit1": unit1,
		"layer1": layer1,
		"layer2": layer2,
		"unit2": unit2
	};

	sent_message(msg);
}

// Convert message to json and sent it to backend.
function sent_message(msg) {
	var json_msg = JSON.stringify(msg)
	ws.send(json_msg)
}

$(document).ready(function() {
	$("#layer-selector").change(function() {
		layer = parseInt(this.value);
		if (vis == "intralayer-projection") {
			request_intralayer_projection();
		} else if (this.value == "hiddenstate-projection") {
			request_hiddenstate_projection();
		}
	});
});

$(document).ready(function() {
	$("#sorting-selector").change(function() {
		sorting = this.value;
		if (vis == "intralayer-projection") {
			request_intralayer_projection();
		} 
	});
});

$(document).ready(function() {
	$("#projection-selector").change(function() {
		projection = this.value;
		if (vis == "intralayer-projection") {
			request_intralayer_projection();
		} else if (vis == "interlayer-projection") {
			request_interlayer_projection();
		} else if (this.value == "hiddenstate-projection") {
			request_hiddenstate_projection();
		}
	});
});

$(document).ready(function() {
	$("#technique-selector").change(function() {
		vis = this.value;
		if (this.value == "intralayer-projection") {
			request_intralayer_projection();
		} else if (this.value == "interlayer-projection") {
			request_interlayer_projection();
		} else if (this.value == "hiddenstate-projection") {
			request_hiddenstate_projection();
		}
	});
});

// Close Websocket callback.
ws.onclose = function(evt) {
	console.log("***Connection Closed***");
};