import json

from keras.callbacks import Callback

"""
	ModelLogger: Callback to save the information about the training of a model. It saves one JSON 7
	file with the model configuration and several hdf5 files with the model's learnable parameters for 
	each epoch of the training process.

	Arguments:
		- folder: folder where to save the files.
"""
class ModelLogger(Callback):
	def __init__(self, folder):
		self.folder = folder

	# When the training begins, saves the initial weights and the model configuration.
	def on_train_begin(self, logs={}):
		model_json = self.model.to_json()
		with open(self.folder + '/model.json', 'w') as outfile:
			json.dump(model_json, outfile)

		self.model.save_weights(self.folder + '/weights_epoch0.hdf5')

	# At the end of each epoch, saves the current weights
	def on_epoch_end(self, epoch, logs={}):
		self.model.save_weights(self.folder + '/weights_epoch' + str(epoch + 1) + '.hdf5')
