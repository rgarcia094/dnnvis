import numpy as np
import random

from keras.datasets import cifar10
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dense, Dropout, Flatten
from keras.utils import np_utils
from keras.optimizers import rmsprop

from callbacks import ModelLogger

# Set random seed for reproducibility.
np.random.seed(0)
random.seed(0)

# Set hyperparameters.
batch_size = 32				# In each iteration, we consider 32 training examples at once.
num_epochs = 200			# We iterate 200 times over the entire training set.
kernel_size = 3				# We use 3x3 kernels throughout the convolutions.
pool_size = 2				# We use 2x2 pooling throughout the pooling layers.
conv_depth_1 = 32			# We initially have 32 kernels per conv. layer.
conv_depth_2 = 64			# Switching to 64 after the first pooling layer.
conv_depth_3 = 128			# Switching to 128 after the second pooling layer.
drop_prob_1 = 0.25			# Dropout after pooling with probability 0.25
drop_prob_2 = 0.5			# Dropout in the dense layer with probability 0.5
hidden_size = 8				# The dense layer will have 512 neurons

# Fetch CIFAR-10 data.
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

# Dataset information.
num_train, height, width, depth = x_train.shape # There are 50000 training examples in CIFAR-10.
num_test = x_test.shape[0] 						# There are 10000 test examples in CIFAR-10.
num_classes = np.unique(y_train).shape[0] 		# There are 10 image classes.

# Normalise data to [0, 1] range.
x_train = x_train.astype('float32') 
x_test = x_test.astype('float32')
x_train /= np.max(x_train)
x_test /= np.max(x_test)

# One-hot encode the labels.
y_train = np_utils.to_categorical(y_train, num_classes) # One-hot encode the labels
y_test = np_utils.to_categorical(y_test, num_classes) # One-hot encode the labels

# Define layers of the first convolutional step.
conv_1 = Conv2D(conv_depth_1, (kernel_size, kernel_size), padding='same', activation='relu', input_shape=(height, width, depth))
conv_2 = Conv2D(conv_depth_1, (kernel_size, kernel_size), activation='relu')
pool_1 = MaxPooling2D(pool_size=(pool_size, pool_size))
drop_1 = Dropout(drop_prob_1)

# Define layers of the second convolutional step.
conv_3 = Conv2D(conv_depth_2, (kernel_size, kernel_size), padding='same', activation='relu')
conv_4 = Conv2D(conv_depth_2, (kernel_size, kernel_size), activation='relu')
pool_2 = MaxPooling2D(pool_size=(pool_size, pool_size))
drop_2 = Dropout(drop_prob_1)

# Define layers of the third convolutional step.
conv_5 = Conv2D(conv_depth_3, (kernel_size, kernel_size), padding='same', activation='relu')
conv_6 = Conv2D(conv_depth_3, (kernel_size, kernel_size), activation='relu')
pool_3 = MaxPooling2D(pool_size=(pool_size, pool_size))
drop_3 = Dropout(drop_prob_1)
flat_1 = Flatten()

# Instantiate the model.
model = Sequential()
model.add(conv_1)
model.add(conv_2)
model.add(pool_1)
model.add(drop_1)
model.add(conv_3)
model.add(conv_4)
model.add(pool_2)
model.add(drop_2)
model.add(conv_5)
model.add(conv_6)
model.add(pool_3)
model.add(drop_3)
model.add(flat_1)

# Load weights from previous trained model.
model.load_weights('../models/cifar10-1/weights_epoch65.hdf5', by_name=True)

# Predict output of convolutional layers for both datasets.
c_train = model.predict(x_train)
c_test = model.predict(x_test)

# Define layers of the classification step.
dens_1 = Dense(hidden_size, activation='relu', input_shape=c_train.shape[1:])
drop_4 = Dropout(drop_prob_2)
output = Dense(num_classes, activation='softmax')

# Instantiate and train the dense part of the model.
model = Sequential()
model.add(dens_1)
model.add(drop_4)
model.add(output)

opt = rmsprop(lr=0.0001, decay=1e-6)
model.compile(loss='categorical_crossentropy', optimizer=opt, metrics=['accuracy'])
model.fit(c_train, y_train, batch_size=batch_size, epochs=num_epochs, validation_split=0.1, shuffle=True, callbacks=[ModelLogger(folder='cifar10-1_tl')])

# Evaluate the model.
scores = model.evaluate(c_test, y_test, verbose=2)
print("Accuracy: %.2f%%" % (scores[1]*100))
