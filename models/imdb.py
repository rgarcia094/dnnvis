import numpy as np
import random

from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense, LSTM
from keras.layers.convolutional import Conv1D, MaxPooling1D
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence

from callbacks import ModelLogger

# Set random seed for reproducibility.
np.random.seed(0)
random.seed(0)

# Set hyperparameters.
top_words = 5000				# We only consider the top 5k words in the entire dataset.
max_review_length = 500			# Each review must have 500 words.
embedding_vector_length = 32	# Embedding layer will output a different 32-D vector for each word.
batch_size = 64					# In each iteration, we consider 64 training examples at once.
num_epochs = 200				# We iterate 200 times over the entire training set.
conv_depth_1 = 32				# We initially have 32 kernels per conv. layer.
conv_kernel_1 = 3				# Kernels are applied in groups of three words.
pool_size_1 = 2					# After convolution, sequence size is halved. 
lstm_units = 100				# Number of LSTM units in each LSTM layer.

# Load the dataset but only keep the top n words, zero the rest.
(x_train, y_train), (x_test, y_test) = imdb.load_data(num_words=top_words)

# Truncate and pad input sequences (fill with zeros if fewer words then max length).
x_train = sequence.pad_sequences(x_train, maxlen=max_review_length)
x_test = sequence.pad_sequences(x_test, maxlen=max_review_length)

# Instantiate the layers of the model.
model = Sequential()
model.add(Embedding(input_dim=top_words, output_dim=embedding_vector_length, input_length=max_review_length))
model.add(Conv1D(filters=conv_depth_1, kernel_size=conv_kernel_1, padding='same', activation='relu'))
model.add(MaxPooling1D(pool_size=pool_size_1))
model.add(LSTM(units=lstm_units, return_sequences=True))
model.add(LSTM(units=lstm_units))
model.add(Dense(units=1, activation='sigmoid'))

# Train the model,
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(x_train, y_train, batch_size=batch_size, epochs=num_epochs, validation_split=0.1, callbacks=[ModelLogger(folder='imdb')])

# Evaluate the model.
scores = model.evaluate(x_test, y_test, verbose=2)
print("Accuracy: %.2f%%" % (scores[1]*100))
