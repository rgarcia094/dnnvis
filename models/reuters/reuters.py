import numpy as np
import random

from keras.datasets import reuters
from keras.models import Model, Sequential
from keras.layers import Input, Conv1D, MaxPooling1D, Dense, Dropout, LSTM, Embedding
from keras.preprocessing import sequence
from keras.utils import np_utils

from callbacks import ModelLogger

# Set random seed for reproducibility.
np.random.seed(0)
random.seed(0)

# Set hyperparameters.
batch_size = 32				# In each iteration, we consider 32 training examples at once.
num_epochs = 200			# We iterate 200 times over the entire training set.
top_words = 5000			# We only consider the top 5k words in the entire dataset.
max_sequence_length = 500	# Each training example will have exactly 500 words.
embedding_length = 32		# Embedding layer will output a different 32-D vector for each word. 
conv_depth_1 = 32			# We initially have 32 kernels per conv. layer.
conv_kernel_1 = 3			# Kernels are applied in groups of three words.
pool_size_1 = 2				# After convolution, sequence size is halved. 

# Load wirenews dataset.
(x_train, y_train), (x_test, y_test) = reuters.load_data(num_words=top_words, index_from=0)

# Dataset information.
num_train = x_train.shape[0] 				# There are 8982 training examples in Reuters.
num_test = x_test.shape[0] 					# There are 2246 test examples in Reuters.
num_classes = np.unique(y_train).shape[0] 	# There are 46 classes.

# Truncate and pad input sequences (fill with zeros if fewer words then max length).
x_train = sequence.pad_sequences(x_train, maxlen=max_sequence_length)
x_test = sequence.pad_sequences(x_test, maxlen=max_sequence_length)

# One-hot encode labels.
y_train = np_utils.to_categorical(y_train, num_classes)
y_test = np_utils.to_categorical(y_test, num_classes) 

# Define model layers.
model = Sequential()
model.add(Embedding(input_dim=top_words, output_dim=embedding_length, input_length=max_sequence_length))
model.add(Conv1D(filters=conv_depth_1, kernel_size=conv_kernel_1, padding='same', activation='relu'))
model.add(MaxPooling1D(pool_size=pool_size_1))
model.add(LSTM(units=100, return_sequences=True))
model.add(LSTM(units=100))
model.add(Dense(units=46, activation='sigmoid'))

# Compile and train the model.
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(x_train, y_train, batch_size=batch_size, epochs=num_epochs, validation_split=0.1, callbacks=[ModelLogger(folder='reuters')])

# Evaluate the model.
model.evaluate(x_test, y_test, verbose=2)