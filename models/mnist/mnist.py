import numpy as np
import random

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.utils import np_utils

from callbacks import ModelLogger

# Set random seed for reproducibility.
np.random.seed(0)
random.seed(0)

# Set hyperparameters.
batch_size = 128	# In each iteration, we consider 128 training examples at once.
num_epochs = 50		# We iterate 50 times over the entire training set.
kernel_size = 3		# We use 3x3 kernels throughout the convolutions.
pool_size = 2		# We use 2x2 pooling throughout the pooling layers.
conv_depth_1 = 32	# We initially have 32 kernels per conv. layer.
conv_depth_2 = 64	# Switching to 64 after the first pooling layer.
drop_prob_1 = 0.25	# Dropout after pooling with probability 0.25
drop_prob_2 = 0.5	# Dropout in the dense layer with probability 0.5
hidden_size = 128	# The dense layer will have 128 neurons

# Fetch MNIST data, split between train and test sets.
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# Dataset information.
num_train, height, width = x_train.shape
num_test = x_test.shape[0] 						
num_classes = np.unique(y_train).shape[0]

# Reshape to 3D.
x_train = x_train.reshape((num_train, height, width, 1))
x_test = x_test.reshape((num_test, height, width, 1))

# Normalise data to [0, 1] range.
x_train = x_train.astype('float32') 
x_test = x_test.astype('float32')
x_train /= np.max(x_train)
x_test /= np.max(x_test)

# One-hot encode the labels.
y_train = np_utils.to_categorical(y_train, num_classes)
y_test = np_utils.to_categorical(y_test, num_classes)

model = Sequential()
model.add(Conv2D(filters=conv_depth_1, kernel_size=(kernel_size, kernel_size), activation='relu', input_shape=(height, width, 1)))
model.add(Conv2D(filters=conv_depth_2, kernel_size=(kernel_size, kernel_size), activation='relu'))
model.add(MaxPooling2D(pool_size=(pool_size, pool_size)))
model.add(Dropout(drop_prob_1))
model.add(Flatten())
model.add(Dense(units=hidden_size, activation='relu'))
model.add(Dropout(drop_prob_2))
model.add(Dense(units=num_classes, activation='softmax'))

# Instantiate and train the model.
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(x_train, y_train, batch_size=batch_size, epochs=num_epochs, validation_split=0.1, callbacks=[ModelLogger(folder='mnist')])

# Evaluate the model.
scores = model.evaluate(x_test, y_test, verbose=2)
print("Accuracy: %.2f%%" % (scores[1]*100))