import numpy as np
import random

from keras.datasets import reuters
from keras.models import Model, Sequential
from keras.layers import Input, Conv1D, MaxPooling1D, Dense, Dropout, LSTM, Embedding, Dropout
from keras.preprocessing import sequence
from keras.utils import np_utils

from callbacks import ModelLogger

# Set random seed for reproducibility.
np.random.seed(0)
random.seed(0)

# Set hyperparameters.
batch_size = 32				# In each iteration, we consider 32 training examples at once.
num_epochs = 200			# We iterate 200 times over the entire training set.
top_words = 10000			# We only consider the top 10k words in the entire dataset.
max_sequence_length = 512	# Each training example will have exactly 512 words.
embedding_length = 32		# Embedding layer will output a different 32-D vector for each word. 
conv_depth_1 = 128			# We initially have 128 kernels per conv. layer.
conv_kernel_1 = 3			# Kernels are applied in groups of three words.
pool_size_1 = 2				# After convolution, sequence size is halved. 
lstm_units = 256			# Number of LSTM units in each LSTM layer.
hidden_size = 128			# The dense layer will have 128 neurons
drop_prob_1 = 0.25			# Dropout after pooling with probability 0.25
drop_prob_2 = 0.5			# Dropout in the dense layer with probability 0.5

# Load wirenews dataset.
(x_train, y_train), (x_test, y_test) = reuters.load_data(num_words=top_words, index_from=0)

# Dataset information.
num_train = x_train.shape[0] 				# There are 8982 training examples in Reuters.
num_test = x_test.shape[0] 					# There are 2246 test examples in Reuters.
num_classes = np.unique(y_train).shape[0] 	# There are 46 classes.

# Truncate and pad input sequences (fill with zeros if fewer words then max length).
x_train = sequence.pad_sequences(x_train, maxlen=max_sequence_length)
x_test = sequence.pad_sequences(x_test, maxlen=max_sequence_length)

# One-hot encode labels.
y_train = np_utils.to_categorical(y_train, num_classes)
y_test = np_utils.to_categorical(y_test, num_classes) 

# Define model layers.
model = Sequential()
model.add(Embedding(input_dim=top_words, output_dim=embedding_length, input_length=max_sequence_length))
model.add(Conv1D(filters=conv_depth_1, kernel_size=conv_kernel_1, padding='same', activation='relu'))
model.add(MaxPooling1D(pool_size=pool_size_1))
model.add(Dropout(drop_prob_1))
model.add(LSTM(units=lstm_units, return_sequences=True))
model.add(LSTM(units=lstm_units))
model.add(Dropout(drop_prob_2))
model.add(Dense(units=hidden_size, activation='relu'))
model.add(Dropout(drop_prob_2))
model.add(Dense(units=num_classes, activation='softmax'))

# Compile and train the model.
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.fit(x_train, y_train, batch_size=batch_size, epochs=num_epochs, validation_split=0.1, callbacks=[ModelLogger(folder='reuters3')])

# Evaluate the model.
scores = model.evaluate(x_test, y_test, verbose=2)
print("Accuracy: %.2f%%" % (scores[1]*100))